# Author: Nick Sauerwein
#         Matthias Frey
# Date:   2016
#
# Creates the window and does some phase space plots.
# Here you can add more features.
#
# Python naming convention:
#   All functions and variables that are given with two
#   leading underscores "__" should be considered as
#   private member functions, respectively, variables.
#
# Public member functions:
#   - getSlider
#   - doPhaseSpacePlot
#
# Private member functions:
#   - __initSlider
#   - __doUpdate
#   - __doElementPlot
#   - __doTwissPlot
#

import matplotlib.pylab as plt
import numpy as np
from itertools import *
from matplotlib.widgets import Slider
import matplotlib.gridspec as gridspec

import Global as g


class Window:
    
    # -------------------------------------------------------------------
    # @param bunch speicifies a Bunch object
    # @param ac specifies an Accelerator object
    # -------------------------------------------------------------------
    def __init__(self, bunch, ac):
        self.__bunch = bunch
        self.__ac = ac
        
        self.__fig = plt.figure(figsize=(16, 9))
        
        # axes for phase space plots
        main_grid = gridspec.GridSpec(3, 1)
        
        phase_grid = gridspec.GridSpecFromSubplotSpec(1, 4, main_grid[0,0])
        self.axs = [plt.subplot(phase_grid[0,0]),
                    plt.subplot(phase_grid[0,1]),
                    plt.subplot(phase_grid[0,2]),
                    plt.subplot(phase_grid[0,3])]
        
        
        # axis for Twiss parameters
        twiss_grid = gridspec.GridSpecFromSubplotSpec(3, 1, main_grid[1,0])
        ax_twiss = [plt.subplot(twiss_grid[0,0]),
                    plt.subplot(twiss_grid[1,0]),
                    plt.subplot(twiss_grid[2,0])]
        
        #plt.tight_layout()
        self.__fig.subplots_adjust(left=0.1)
        
        # add_axes( [left, bottom, width, height] )
        self.__initSlider(self.__fig.add_axes([0.1, 0.25, 0.8, 0.02]))
        
        # add plots
        self.__doUpdate(0.0)
        self.__doTwissPlot(ax_twiss)
        self.__doElementPlot(self.__fig.add_axes([0.1,0.2, 0.8, 0.02],
                                                 yticklabels = [], yticks = [],
                                                 xticks = [])
        )
    
    
    # -------------------------------------------------------------------
    # draw the slider
    # @param ax is the slider axis
    # -------------------------------------------------------------------
    def __initSlider(self, ax):
        self.__slider = Slider(ax, 'pos [m]', 0,
                               self.__ac.getLength() * g.nrep,
                               valinit = 0)
        self.__slider.on_changed(self.__doUpdate)
    
    
    # -------------------------------------------------------------------
    # slider needs to be returned to main function
    # otherwise it does not work
    # -------------------------------------------------------------------
    def getSlider(self):
        return self.__slider
    
    
    # -------------------------------------------------------------------
    # is called when moving the slider
    # update all plots
    # @param pos is the slider position [m]
    # -------------------------------------------------------------------
    def __doUpdate(self, pos):
        
        particles = self.__ac.getBunchAtPos(pos, self.__bunch.getIniParticles())
        
        # particles[:,0] all x
        # particles[:,1] all p_x
        # particles[:,2] all y
        # particles[:,3] all p_y
        # particles[:,4] all z
        # particles[:,5] all p_z
        
        # (x, p_x)
        self.doPhaseSpacePlot(particles[:,0], particles[:,1],
                              self.axs[0], r'$x\ [mm]$', r'$p_{x}\ [mrad]$',
                              'blue')
        
        # (y, p_y)
        self.doPhaseSpacePlot(particles[:,2], particles[:,3],
                              self.axs[1], r'$y\ [mm]$', r'$p_{y}\ [mrad]$',
                              'red')
        
        # (z, p_z)
        self.doPhaseSpacePlot(particles[:,4], particles[:,5],
                              self.axs[2], r'$z\ [mm]$', r'$p_{z}\ [mrad]$',
                              'green')
        
        # (x, y)
        self.doPhaseSpacePlot(particles[:,0], particles[:,2],
                              self.axs[3], r'$x\ [mm]$', r'$y\ [mm]$',
                              'orange')
    
    
    # -------------------------------------------------------------------
    # draw the particles in specified (var1, var2) phase space
    # @param var1 is the data for the x-axis
    # @param var2 is the data for the y-axis
    # @param ax is the subplot to which we draw
    # @param xlab is the x-axis label
    # @param ylab is the y-axis label
    # -------------------------------------------------------------------
    def doPhaseSpacePlot(self, var1, var2, ax, xlab, ylab, col = 'red'):
        ax.cla() # clear
        ax.scatter(var1 , var2, s = 1, color = col)
        ax.set_xlabel(xlab)
        ax.set_ylabel(ylab)
        ax.set_ylim(-g.ylim, g.ylim)
        ax.set_xlim(-g.xlim, g.xlim)
    
    
    # -------------------------------------------------------------------
    # plot the beamline elements
    # it calls the plotElements-function of Accelerator.
    # @param ax is the axis where to draw
    # -------------------------------------------------------------------
    def __doElementPlot(self, ax):
        ax.set_xlim(0, self.__ac.getLength() * g.nrep)
        ax.set_ylim(0, 0.5)
        self.__ac.plotElements(ax)


    
    # -------------------------------------------------------------------
    # plot the Twiss parameters in horizontal and vertical direction
    # plot the dispersion
    # @param ax is the axes where to plot
    # -------------------------------------------------------------------
    def __doTwissPlot(self, axs):
        # Sorry for this ugly coding!
        
        pos = np.linspace(0, self.__ac.getLength() * g.nrep, 1000)
        
        alpha = np.empty((0,3), float)
        beta = np.empty((0,3), float)
        disp = []
        for p in pos:
            particles = self.__ac.getBunchAtPos(p, self.__bunch.getIniParticles())
            self.__bunch.updateParameters(particles)
            twiss = self.__bunch.calcTwiss()
            alpha = np.append(alpha, np.array([twiss[0,:]]), axis=0)
            beta = np.append(beta, np.array([twiss[1,:]]), axis=0)
        
            map = self.__ac.getMapAtPos(p)
            disp = np.append(disp, map[5, 0])
        
        col = ['blue', 'red', 'green']
        lab_alpha = [r'$\alpha_{x}$', r'$\alpha_{y}$', r'$\alpha_{z}$']
        lab_beta = [r'$\beta_{x}$', r'$\beta_{y}$', r'$\beta_{z}$']
        for i in range(3):
            axs[0].plot(pos, alpha[:, i], col[i], label = lab_alpha[i])
            axs[1].plot(pos, beta[:, i], col[i], label = lab_beta[i])
            
        axs[0].set_xlabel(r'$\alpha$')
        axs[2].plot(pos, disp, col[2], label=r'$D\ [m]$')
        
        for i in range(3):
            axs[i].legend(loc='center left', bbox_to_anchor=(-0.08, 0.5))
        
        axs[2].set_xlabel('pos [m]')

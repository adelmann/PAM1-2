# Author:   Matthias Frey
# Date:     28. August 2016
#
# Specifies a bunch
# Ordering: (x, px, z, py, z, pz)
#
# Python naming convention:
#   All functions and variables that are given with two
#   leading underscores "__" should be considered as
#   private member functions, respectively, variables.
#
# Public member functions:
#   - calcTwiss
#   - getIniParticles
#   - getSigmaMatrix
#   - getEmittances
#   - updateParameters
#   - gaussian
#
# Private member functions:
#   - __calcMoments
#   - __calcEmittance
#

import numpy as np

class Bunch:
    
    # -------------------------------------------------------------------
    # member variables:
    # - particles:  initial particles
    # - numParticles
    # - emit: emittances in x, y and z
    # - sigma: second moment matrix
    # -------------------------------------------------------------------
    def __init__(self):
        
        # default initialization
        self.__particles = []
        self.__numParticles = 0
        self.__emit = np.zeros( shape = (1, 3) )
        self.__sigma = np.zeros( shape = (6, 6) )
    
    
    # -------------------------------------------------------------------
    # compute the second moment matrix
    # @param particles where each row contains (x, px, y, py, z, pz)
    # -------------------------------------------------------------------
    def __calcMoments(self, particles):
        
        inv = 1.0 / self.__numParticles
        
        mean = np.zeros( (1, 6) )
        for i in range(0, 6):
            mean[:, i] = sum( self.__particles[:, i] ) * inv
        
        for i in range(0, 6):
            for j in range(i, 6):
                self.__sigma[i, j] = \
                self.__sigma[j, i] = inv * sum( particles[:, i] * 
                                                particles[:, j] ) \
                                    - mean[:, i] * mean[:, j]
    
    
    # -------------------------------------------------------------------
    # update emittances [emitx, emity, emitz]
    # -------------------------------------------------------------------
    def __calcEmittances(self):
        for i in range(3):
            j = 2 * i
            self.__emit[:, i] = np.sqrt(self.__sigma[j, j] * 
                                        self.__sigma[j + 1, j + 1] - 
                                        self.__sigma[j, j + 1] ** 2)
    
    
    # -------------------------------------------------------------------
    # returns a 3x3 matrix
    # [[alphax, alphay, alphaz],
    #  [betax, betay, betaz],
    #  [gammax, gammay, gammaz]]
    # -------------------------------------------------------------------
    def calcTwiss(self):
        alpha = np.zeros( shape = (3, 1) )
        beta = np.zeros( shape = (3, 1) )
        gamma = np.zeros( shape = (3, 1) )
        
        for i in range(3):
            inv = 1.0 / self.__emit[:, i]
            j = 2 * i
            beta[i] = self.__sigma[j, j] * inv
            alpha[i] = - self.__sigma[j, j + 1] * inv
            gamma[i] = self.__sigma[j + 1, j + 1] * inv
        
        return np.reshape([ alpha, beta, gamma ], (3,3))
        
    
    # -------------------------------------------------------------------
    # return all initial particles
    # each row is a particle (x, px, y, py, z, pz)
    # -------------------------------------------------------------------
    def getIniParticles(self):
        return self.__particles
    
    
    # -------------------------------------------------------------------
    # returns the 6x6 covariance matrix
    # -------------------------------------------------------------------
    def getSigmaMatrix(self):
        return self.__sigma
    
    
    # -------------------------------------------------------------------
    # returns a vector [emitx, emity, emitz]
    # -------------------------------------------------------------------
    def getEmittances(self):
        return self.__emit
    
    
    # -------------------------------------------------------------------
    # update the emittances, sigma matrix
    # @param particles where each row is a particle (x, px, y, py, z, pz)
    # -------------------------------------------------------------------
    def updateParameters(self, particles):
        self.__calcMoments(particles)
        self.__calcEmittances()
    
    
    # -------------------------------------------------------------------
    # returns a gaussian bunch where row is particle (x, px, y, py, z, pz)
    # @param n is the number of particles
    # @param mu specifies mean
    # @param C is the covariance matrix
    # -------------------------------------------------------------------
    def gaussian(self, n, mu, C):
        self.__particles = np.random.multivariate_normal(mu, C, n)
        self.__numParticles = n

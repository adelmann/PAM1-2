# Author:   Nick Sauerwein
# Date:     2016

from Elements import *
import matplotlib.patches as patches
from numpy.linalg import matrix_power

class Accelerator:
    # Initialize the accelerator
    def __init__(self, elements):
        self.elements = elements
        
        self.var = { 0 : r'$x$', 1 : r'$p_{x}$',
                     2 : r'$y$', 3 : r'$p_{y}$',
                     4 : r'$z$', 5 : r'$p_{z}$'
                    }
        
        # https://docs.python.org/3/reference/expressions.html#generator-expressions
        self.length = np.sum(device.L for device in self.elements)
        
        self.mats = [] # stores all matrices in a list
        self.distances = [0] # stores all element distances from the origin in a lists
        
        self.make()
    
    def make(self):
        # construct all matrices
        for k in range(len(self.elements)):
                mat = np.eye(6)
                distance = 0
                for device in self.elements[:k+1]:
                        distance += device.L
                        mat = mat @ device.get()
                        
                self.mats = self.mats + [mat]
                self.distances = self.distances + [distance]
    
    # returns the beam at a certain position
    # @param z is the position in longitudinal direction where to evaluate
    # @param beam is the initial beam
    def getBeamAtPos(self, z, beam):
        if z > self.length:
                beam = beam @ matrix_power(self.mats[-1],int(z/self.length))
                z = z%self.length
                if z == 0:
                        return beam
        k = np.searchsorted(self.distances,z,side = 'left')-1
        if z == 0:
                k = 0
        if k != 0:
                beam = beam @ self.mats[k-1]
        
        beam = beam @ self.elements[k].get(z-self.distances[k])
        return beam
    
    # returns the transfer matrix for a certain position
    # @ param z is the position to which the map should be computed
    def getMapAtPos(self, z):
        mat = np.eye(6)
        if z > self.length:
                mat = mat @ matrix_power(self.mats[-1],int(z/self.length))
                z = z%self.length
                if z == 0:
                        return mat
        k = np.searchsorted(self.distances,z,side = 'left')-1
        if z == 0:
                k = 0
        if k != 0:
                mat = mat @ self.mats[k-1]

        return mat @ self.elements[k].get(z-self.distances[k])
        
        #return mat

    def statz(self, z,beam):
        beams = map(lambda s: self.getBeamAtPos(s, beam),z)
        cc = []
        mm = []
        for zz in beams:
                c = np.cov(np.transpose(zz))
                m = np.mean(np.transpose(zz),axis = 1)
                cc += [c]
                mm += [m]
        return np.array(mm),np.array(cc)

    # return the i-th variable as string (x, px, y, py, z, pz)
    def getVarLabel(self, i):
        return self.var[i]
    
    def plot(self, ax, nrep):
        x = nrep * self.length
        n = int(x/self.length)+1
        for r in self.length*np.arange(n):
                for k in range(len(self.elements)):
                    ax.add_patch(patches.Rectangle((self.distances[k] + r, 0),
                                                   self.elements[k].L,
                                                   0.5,
                                                   color = self.elements[k].color
                                                   )
                    )

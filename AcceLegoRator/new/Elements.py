# Author:   Nick Sauerwein, Matthias Frey
# Date:     28. August 2016
#
# This file contains all 6x6 matrices of a particle accelerator. Additional
# elements have to inherit from the base class ElementBase.
# The element classes are implemented in such a way that they do not store the matrices as member variables
# but construct them when calling the getter function.

# Member functions of ElementBase:
# - constructor:    initialize beta_0 and gamma_0 (known to all instances)
# - get(L = None):  return the 6x6 matrix at a specified location

from abc import ABC, abstractmethod

import numpy as np
from Physics import *

import Global as g  # global parameters specified by user in the main function

# ------------------------------------------------------------------------------------------------------------------------
# Base class of all elements. Additional elements need to inherit from it.
# ------------------------------------------------------------------------------------------------------------------------
class ElementBase:
    
    @abstractmethod
    def __init__(self):
        # set values of private member variables (are known by all derived object instances)
        self.beta_0 = Physics.getBeta(g.gamma_0)
    
    # return the 6x6 matrix (the position can be specified)
    @abstractmethod
    def get(self, L = None):
        pass
    
    # used in Dipole and Quadrupole
    def rotation(self, phi):
        cs = np.cos(phi)
        s  = np.sin(phi)
        
        return np.array([[cs,   0,  s,  0,  0,  0],
                         [0,    cs, 0,  s,  0,  0],
                         [-s,   0,  cs, 0,  0,  0],
                         [0,    -s, 0,  cs, 0,  0],
                         [0,    0,  0,  0,  1,  0],
                         [0,    0,  0,  0,  0,  1]])
    
    # return the name of the element
    @abstractmethod
    def getType(self):
        pass
    
# -------------------------------------------------------------------------------------------------------------------------
# Drift
# -------------------------------------------------------------------------------------------------------------------------
class Drift(ElementBase):
    
    # constructor
    def __init__(self, L):
        
        # call base class constructor (--> initialize beta_0 and gamma_0)
        ElementBase.__init__(self)
        
        # set values of private member variables
        self.L = L
        self.color = 'white' # color for plotting
        
        
    # return 6x6 matrix
    def get(self, L = None):
        
        # check if a length is specified, if not take the one of object construction
        if L is None:
            L = self.L
        
        f = L / (self.beta_0 * g.gamma_0) ** 2
        
        return np.array([[1, L, 0, 0, 0, 0],
                         [0, 1, 0, 0, 0, 0],
                         [0, 0, 1, L, 0, 0],
                         [0, 0, 0, 1, 0, 0],
                         [0, 0, 0, 0, 1, f],
                         [0, 0, 0, 0, 0, 1]]).T

    def getType(self):
        return "Drift"
    
# -------------------------------------------------------------------------------------------------------------------------
# Dipole 
# -------------------------------------------------------------------------------------------------------------------------    
class Dipole(ElementBase):
    
    # initialize a dipole element with
    # @param L is the length [m]
    # @param b0 is the absolute magnetic field strength
    # @param phi is the rotation angle for skewness
    def __init__(self, L, b0, phi):
        
        # call base class constructor (--> initialize beta_0 and gamma_0)
        ElementBase.__init__(self)
        
        # set values of private member variables
        self.L = L
        self.b0 = b0    
        self.phi = phi
        self.color = 'blue' # color for plotting
        
        
    # return 6x6 matrix
    def get(self, L = None):
        
        # check if a length is specified, if not take the one of object construction
        if L is None:
            L = self.L
        
        c = Constants.clight    # m / s
        q = Constants.echarge   # C
        
        # reference momentum
        P_0 = g.mass * 1e6 * q / c * np.sqrt( g.gamma_0 ** 2 - 1) # MeV/c**2
        
        # normalised dipole field strength k_0
        # choice: reference trajectory matched to dipole strength --> w = h = k_0
        # q is the charge of the reference particle
        w = q / P_0 * self.b0
        
        r = self.rotation(self.phi)
        r_i = self.rotation(-self.phi)
        
        # fringe field of dipole
        # psi is the rotation of the pole face from the reference trajectory
        psi = 0.5 * L * q * self.b0 / ( 2 * np.pi * P_0 )
        k1 = -w * np.tan( psi )
        
        fringe = np.array([[1,      0,  0,  0,  0,  0],
                           [-k1,    1,  0,  0,  0,  0],
                           [0,      0,  1,  0,  0,  0],
                           [0,      0,  k1, 1,  0,  0],
                           [0,      0,  0,  0,  1,  0],
                           [0,      0,  0,  0,  0,  1]])
        
        
        # dipole matrix
        cs   = np.cos(w * L)
        s    = np.sin(w * L)
        f    = L / (self.beta_0 * g.gamma_0) ** 2
        ibet = 1.0 / self.beta_0
        
        di = np.array([[cs,         s / w,              0,  0,  0,  (1 - cs) / w * ibet],
                       [-w * s,     cs,                 0,  0,  0,  s * ibet],
                       [0,          0,                  1,  L,  0,  0],
                       [0,          0,                  0,  1,  0,  0],
                       [-s * ibet,  -(1-cs) / w * ibet, 0,  0,  1,  f - (w * L - s) / w * ibet ** 2],
                       [0,          0,                  0,  0,  0,  1]])
        
        return (r @ fringe @ di @ fringe @ r_i).T
    
    def getType(self):
        return "Dipole"

# -------------------------------------------------------------------------------------------------------------------------
# Quadrupole 
# -------------------------------------------------------------------------------------------------------------------------            
class Quadrupole(ElementBase):
    
    # initialize a quadrupole element with
    # @param L is the length [m]
    # @param b0 is the absolute magnetic field strength
    # @param phi is the angle for the skewness
    def __init__(self, L, b0, phi):
        
        # call base class constructor (--> initialize beta_0 and gamma_0)
        ElementBase.__init__(self)
        
        # set values of private member variables
        self.L = L
        self.b0 = b0
        self.phi = phi
        self.color = 'red' # color for plotting
            
            
    # return 6x6 matrix
    def get(self, L = None):
            
        if L is None:
            L = self.L
            
        c = Constants.clight    # m / s
        q = Constants.echarge   # C
        
        # reference momentum
        P_0 = g.mass * 1e6 * q / c * np.sqrt( g.gamma_0 ** 2 - 1) # MeV/c**2
        
        # k1 = q / P0 * b0 > 0
        w = np.sqrt( q /P_0 * self.b0 )
        
        r = self.rotation(self.phi)
        r_i = self.rotation(-self.phi)
        
        # quadrupole matrix
        cs  = np.cos(w * L)
        s   = np.sin(w * L)
        csh = np.cosh(w * L)
        sh  = np.sinh(w * L)
        f   = L / (self.beta_0 * g.gamma_0 ) ** 2
            
        quad = np.array([[cs,       s / w,  0,      0,      0,  0],
                         [-w * s,   cs,     0,      0,      0,  0],
                         [0,        0,      csh,    sh / w, 0,  0],
                         [0,        0,      w * sh, csh,    0,  0],
                         [0,        0,      0,      0,      1,  f],
                         [0,        0,      0,      0,      0,  1]])
            
        return (r @ quad @ r_i).T
    
    def getType(self):
        return "Quadrupole"
    
# -------------------------------------------------------------------------------------------------------------------------
# TODO: Add additional elements here
# -------------------------------------------------------------------------------------------------------------------------

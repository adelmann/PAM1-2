# Author:   Matthias Frey
# Date:     28. August 2016
#
# This file contains global variables that
# are specified by the user in the main function
# and are needed for initializing the accelerator
# elements.

global gamma_0  # relativistic factor (will be computed by Physics.py)
global mass     # particle rest mass (will be set by Physics.py)
global nrep     # number of repetitions of accelerator setting
global xlim     # x limits for phase space plots
global ylim     # y limits for phase space plots
global xdim     # size of figures of View.py in x-direction
global ydim     # size of figures of View.py in y-direction
global ds       # is the step size for the slider in View.py

# Author:   Nick Sauerwein, Matthias Frey
# Date:     28. August 2016
#
# Call by:
#   python3 main.py

import Global as g
from Elements import *
from Beam import *
from Accelerator import *
from View import *
from Physics import *

if __name__ == "__main__":
    
    # ---------------------------------------------------------------
    # specify particle and kinetic energy
    # ---------------------------------------------------------------
    #g.gamma_0 = 4.3789
    ekin = 3100 #250.0 # MeV
    g.mass = Constants.pmass # MeV
    g.gamma_0 = Physics.getGamma(ekin, g.mass)
    
    print('kin. energy = ', ekin, ' MeV')
    print('gamma_0     = ', g.gamma_0)
    print('beta_0      = ', Physics.getBeta(g.gamma_0))
    print('mass        = ', g.mass, ' MeV / c^2')
    
    
    # ---------------------------------------------------------------
    # beamline definition
    # ---------------------------------------------------------------
    #elements = [Drift(1.0)]
    
    elements = [Quadrupole(0.25, 8.9102, 0),
                Drift(1.0),
                Dipole(2.5, 1.39723, 0),
                Drift(1.0),
                Quadrupole(0.5, 8.2400, -0.5 * np.pi),
                Drift(1.0),
                Dipole(2.5, 1.39723, 0),
                Drift(1.0),
                Quadrupole(0.25, 8.9102, 0)]
    
    # create the accelerator
    ac = Accelerator(elements)
    
    # number of repetitions of the beamline
    g.nrep = 10
    
    # plot dimension
    g.xlim = 0.05
    g.ylim = 0.05
    
    # set the dimension of the figures (used in View.py)
    g.xdim = 4.8
    g.ydim = 4.8
    g.ds = 0.1
    
    
    # ---------------------------------------------------------------
    # set Beam parameters
    # ---------------------------------------------------------------
    n = 1000  #number of particles per bunch
    
    ## covariance matrix
    #C = np.array([[0.00001,0,0,0,0,0],
                  #[0,0.00001,0,0,0,0],
                  #[0,0,0.00001,0,0,0],
                  #[0,0,0,0.00001,0,0],
                  #[0,0,0,0,0.0001,0],
                  #[0,0,0,0,0,0.000001]])
    
    C = np.array([[0.00001*beta(0, ac)[0],0,0,0,0,0],
                  [0,0.00001*gamma(0, ac)[0],0,0,0,0],
                  [0,0,0.00001*beta(0, ac)[1],0,0,0],
                  [0,0,0,0.00001*gamma(0, ac)[1],0,0],
                  [0,0,0,0,0.0001,0],
                  [0,0,0,0,0,0.000001]])
    
    mean = [0,0,0,0,0,0] # Center of beam
    
    ## define initial shape of bunch (Gaussian or ellipse)
    inibeam = Beam.gaussian(n, mean, C)
    
    
    ## ---------------------------------------------------------------
    ## create window
    ## ---------------------------------------------------------------
    app = Window(ac, inibeam, master=Tk())
    app.mainloop()
    Tk().destroy()

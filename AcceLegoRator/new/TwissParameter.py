# Author: Nick Sauerwein
# Date:   2016

from Accelerator import *

def getTwiss(mat):
    S = np.array([[0,1],[-1,0]])
    Rx = np.array([[mat[0,0],mat[1,0]],
                   [mat[0,1],mat[1,1]]])
    Ry = np.array([[mat[2,2],mat[3,2]],
                   [mat[2,3],mat[3,3]]])              
    mux = np.arccos(np.trace(Rx)/2)
    muy = np.arccos(np.trace(Ry)/2)
    
    Ax = S @ (- Rx + np.eye(2)*np.cos(mux))/np.sin(mux)
    Ay = S @ (- Ry + np.eye(2)*np.cos(muy))/np.sin(muy)
    
    return [Ax,mux/(2*np.pi)*360],[Ay,muy/(2*np.pi)*360]
    
def beta(z, accel):
    Twiss = getTwiss(accel.mats[-1])
    Ax0_1 = np.linalg.inv(Twiss[0][0])
    Ay0_1 = np.linalg.inv(Twiss[1][0])
    mat = accel.getMapAtPos(z).T
    Axz_1 = mat[:2,:2] @ Ax0_1 @ mat[:2,:2].T
    Ayz_1 = mat[2:4,2:4] @ Ay0_1 @ mat[2:4,2:4].T
    #betax = getTwiss(mat)[0][0][1,1]
    #betay = getTwiss(mat)[1][0][1,1]
    return [Axz_1[0,0],Ayz_1[0,0]]#[betax,betay]#

def alpha(z, accel):
    Twiss = getTwiss(accel.mats[-1])
    Ax0_1 = np.linalg.inv(Twiss[0][0])
    Ay0_1 = np.linalg.inv(Twiss[1][0])
    mat = accel.getMapAtPos(z).T
    Axz_1 = mat[:2,:2] @ Ax0_1 @ mat[:2,:2].T
    Ayz_1 = mat[2:4,2:4] @ Ay0_1 @ mat[2:4,2:4].T
    #betax = getTwiss(mat)[0][0][1,1]
    #betay = getTwiss(mat)[1][0][1,1]
    return [-Axz_1[0,1],-Ayz_1[0,1]]#[betax,betay]#
    
def gamma(z, accel):
    Twiss = getTwiss(accel.mats[-1])
    Ax0_1 = np.linalg.inv(Twiss[0][0])
    Ay0_1 = np.linalg.inv(Twiss[1][0])
    mat = accel.getMapAtPos(z).T
    Axz_1 = mat[:2,:2] @ Ax0_1 @ mat[:2,:2].T
    Ayz_1 = mat[2:4,2:4] @ Ay0_1 @ mat[2:4,2:4].T
    #betax = getTwiss(mat)[0][0][1,1]
    #betay = getTwiss(mat)[1][0][1,1]
    return [Axz_1[1,1],Ayz_1[1,1]]#[betax,betay]#

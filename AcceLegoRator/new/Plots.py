# Author:   Nick Sauerwein
# Date:     2016

import numpy as np
from numpy.linalg import svd
from matplotlib.patches import Ellipse
import matplotlib.animation as animation
import math

# Beam plots
def PlotScatter(x, y, xlabel = '', ylabel = ''):
    # definitions for the axes
    left, width = 0.1, 0.65
    bottom, height = 0.1, 0.65
    bottom_h = left_h = left+width+0.02
    
    rect_scatter = [left, bottom, width, height]
    rect_histx = [left, bottom_h, width, 0.2]
    rect_histy = [left_h, bottom, 0.2, height]
    
    # start with a rectangular Figure
    fig = plt.figure( figsize=(8,8))
    axScatter = plt.axes(rect_scatter)
    axScatter.set_xlabel(xlabel)
    axScatter.set_ylabel(ylabel)
    axHistx = plt.axes(rect_histx)
    axHisty = plt.axes(rect_histy)
    
    # the scatter plot:
    axScatter.scatter(x, y)
    
    mx = np.mean(x)
    my = np.mean(y)
    xmax = np.max(np.abs(x-mx))
    ymax = np.max(np.abs(y-my))
    
    axScatter.set_xlim( (-xmax+mx, xmax+mx) )
    axScatter.set_ylim( (-ymax+my, ymax+my) )
    binx = np.linspace(axScatter.get_xlim()[0],axScatter.get_xlim()[1],30)
    biny = np.linspace(axScatter.get_ylim()[0],axScatter.get_ylim()[1],30)
    axHistx.hist(x,log = False, bins=binx)
    axHistx.grid()
    axHisty.hist(y, log = False, bins=biny, orientation='horizontal')
    axHisty.grid()
    axHistx.set_xlim( axScatter.get_xlim() )
    axHisty.set_ylim( axScatter.get_ylim() )

def PlotScatterBeam(beam,ax):
    tbeam = np.transpose(beam)
    for conf in ax:
            l = PlotScatter(tbeam[conf[0]],tbeam[conf[1]],xlabel = 'coodinate '+str(conf[0]),ylabel = 'coodinate '+str(conf[1]))
    return l

def plotEllipse(mean,cov,ax, edge = 'black', face = 'none'):
    U, s , Vh = svd(cov)
    orient = math.atan2(U[1,0],U[0,0])/np.pi*180
    ellipsePlot = Ellipse(xy=mean, width=2*math.sqrt(s[0]),
                          height=2*math.sqrt(s[1]), angle=orient,facecolor=face, edgecolor=edge)
    ax.add_patch(ellipsePlot);  
 
def beamVideo(beaml,ax,device,para,iter):
    beam = beaml
    left, width = 0.1, 0.65
    bottom, height = 0.1, 0.65
    bottom_h = left_h = left+width+0.02
    
    rect_scatter = [left, bottom, width, height]
    rect_histx = [left, bottom_h, width, 0.2]
    rect_histy = [left_h, bottom, 0.2, height]
    
    # start with a rectangular Figure
    fig = plt.figure( figsize=(8,8))
    axScatter = plt.axes(rect_scatter)
    axScatter.set_xlabel('coordinate'+str(ax[0]))
    axScatter.set_ylabel('coordinate'+str(ax[1]))
    axHistx = plt.axes(rect_histx)
    axHisty = plt.axes(rect_histy)
    beamt = np.transpose(beam)
    x = beamt[ax[0]]
    y = beamt[ax[1]]
    # the scatter plot:
    s1 = axScatter.scatter(x, y)

    mx = np.mean(x)
    my = np.mean(y)
    xmax = np.max(np.abs(x-mx))
    ymax = np.max(np.abs(y-my))


    axScatter.set_xlim( (-xmax+mx, xmax+mx) )
    axScatter.set_ylim( (-ymax+my, ymax+my) )
    binx = np.linspace(axScatter.get_xlim()[0],axScatter.get_xlim()[1],30)
    biny = np.linspace(axScatter.get_ylim()[0],axScatter.get_ylim()[1],30)
    l1 = axHistx.hist(x,log = False, bins=binx)
    axHistx.grid()
    l2 = axHisty.hist(y, log = False, bins=biny, orientation='horizontal')
    axHisty.grid()
    axHistx.set_xlim( axScatter.get_xlim() )
    axHisty.set_ylim( axScatter.get_ylim() )
    def animate(k):
        global beam
        beam = device(beam,para)
        axScatter.cla()
        axHistx.cla()
        axHisty.cla()
        beamt = np.transpose(beam)
        x = beamt[ax[0]]
        y = beamt[ax[1]]
        s1 =  axScatter.scatter(x,y)
        mx = np.mean(x)
        my = np.mean(y)
        xmax = np.max(np.abs(x-mx))
        ymax = np.max(np.abs(y-my))
        binx = np.linspace(axScatter.get_xlim()[0],axScatter.get_xlim()[1],30)
        biny = np.linspace(axScatter.get_ylim()[0],axScatter.get_ylim()[1],30)
        l1 = axHistx.hist(x,log = False, bins=binx)
        l2 = axHisty.hist(y, log = False, bins=biny, orientation='horizontal')
        axScatter.set_xlim( (-xmax+mx, xmax+mx) )
        axScatter.set_ylim( (-ymax+my, ymax+my) )
        axHistx.set_xlim( axScatter.get_xlim() )
        axHisty.set_ylim( axScatter.get_ylim() )
        return s1,l1,l2,
    anim = animation.FuncAnimation(fig, animate, interval= iter)
    plt.show()
    return beam

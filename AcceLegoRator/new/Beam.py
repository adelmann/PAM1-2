# Author:   Nick Sauerwein, Matthias Frey
# Date:     28. August 2016
#
# Specify the shapes of a beam.
# Ordering: (x, px, z, py, z, pz)

import numpy.random as rd

class Beam:
    
    # constructor does nothing
    def __init__(self):
        pass
    
    # return a gaussian beam
    # n:    number of particles
    # mu:   mean
    # C:    covariance matrix
    def gaussian(n, mu, C):
        return rd.multivariate_normal(mu, C, n)

# Author: Nick Sauerwein, Matthias Frey
# Date:   4. September 2016
#
# This file defines the window layout and does the plots.
# Shown plots:
#   - phase space (x, px)
#   - phase space (y, py)
#   - phase space (z, pz)
#   - phase space (x, y)
#   - Twiss parameters, dispersion over whole beamline
#   - x, px, y, py over whole beamline


import matplotlib.pylab as plt

from matplotlib.widgets import Slider

from itertools import *
from Plots import *

from TwissParameter import *
import Global as g

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
import numpy as np
from tkinter import *


class Window(Frame):
    
    # ----------------------------------------------------------------------------------
    # constructor for the main window
    # ----------------------------------------------------------------------------------
    def __init__(self, accelerator, beam, master=None):
        
        # call constructor
        Frame.__init__(self, master)
        
        master.wm_title("AcceLegoRator")
        
        self.master = master
        
        self.ac = accelerator
        self.beam = beam
        
        # figure dimensions
        self.xdim = g.xdim
        self.ydim = g.ydim
        
        self.z = np.linspace(0, self.ac.length * g.nrep,1000)
        
        # set window size to maximum
        # 4. Sept. 2016, http://stackoverflow.com/questions/15981000/tkinter-python-maximize-window
        self.width = master.winfo_screenwidth()
        self.height = master.winfo_screenheight()
        master.geometry("%dx%d+0+0" % (self.width, self.height))
        
        # do plots
        self.doPhaseSpace()
        self.doTwiss()
        self.doLinePlot()
        self.doBeamline()
        
        #
        # widgets:
        #
        panel = PanedWindow(self.master)
        panel.grid(row = 5)
        
        down = Button(self.master, text='prev', command=self.doPrev)
        panel.add(down)
        
        # slider for the phase space plots and position plot
        self.slider = Scale(self.master, fro = 0, to = self.ac.length * g.nrep, length=200,
                            orient=HORIZONTAL, command=self.doUpdate, resolution=g.ds)
        panel.add(self.slider)
        
        up = Button(self.master, text='next', command=self.doNext)
        panel.add(up)
        
        # add additional plots here
        
    # ----------------------------------------------------------------------------------
    # add all function that should be called when moving the slider or pressing  buttons
    # ----------------------------------------------------------------------------------
    def doUpdate(self, value):
        # 4. Sept. 2016, http://stackoverflow.com/questions/5839517/tkinter-call-two-functions
        self.doPhaseSpaceUpdate(float(value))
        self.doPostionUpdate(float(value))
        
        # add more functions here that should be updated by slider
        
    # called when clicking "next" button, increase slider value by one
    def doNext(self):
        self.slider.set( self.slider.get() + g.ds )
    
    # called when clicking "prev" button, decrease slider value by one
    def doPrev(self):
        self.slider.set( self.slider.get() - g.ds )
    
    # ----------------------------------------------------------------------------------
    # Phase space plots
    # ----------------------------------------------------------------------------------
    def doPhaseSpace(self):
        # 4 phase space plots:
        # 1. (x, px)
        # 2. (y, py)
        # 3. (z, pz)
        # 4. (x, y)
        self.fig = np.array([Figure( figsize=(self.xdim, self.ydim), dpi = 100 ),
                             Figure( figsize=(self.xdim, self.ydim), dpi = 100 ),
                             Figure( figsize=(self.xdim, self.ydim), dpi = 100 ),
                             Figure( figsize=(self.xdim, self.ydim), dpi = 100 )
                             ])
        
        for i in range(0, len(self.fig)):
            self.fig[i].subplots_adjust(bottom=0.2, left=0.2)
        
        self.ax = np.array([self.fig[0].add_subplot(1,1,1),
                            self.fig[1].add_subplot(1,1,1),
                            self.fig[2].add_subplot(1,1,1),
                            self.fig[3].add_subplot(1,1,1)
                            ])
        
        self.canvas = np.array([FigureCanvasTkAgg(self.fig[0], master=self.master),
                                FigureCanvasTkAgg(self.fig[1], master=self.master),
                                FigureCanvasTkAgg(self.fig[2], master=self.master),
                                FigureCanvasTkAgg(self.fig[3], master=self.master)
                                ])
        
        for i in range(0, len(self.canvas)):
            self.canvas[i].show()
            self.canvas[i].get_tk_widget().grid(row=1, column=i)
    
    
    # update the phase space plots, is called by the slider
    # @param z is the longitudinal position
    def doPhaseSpaceUpdate(self, z):
        
        # do phase space scatter plots and ellipses
        sol = self.ac.getBeamAtPos(z, self.beam)
        s = np.ones_like(sol[:,1])
        
        mean, cov = self.ac.statz([z], self.beam)
        
        # plots 0, 1, 2
        for i in range(0, len(self.ax)-1):
            self.ax[i].cla()
            self.ax[i].scatter(sol[:, 2*i],sol[:, 2*i+1],s=s)
            self.ax[i].set_xlabel(self.ac.getVarLabel(2*i))
            self.ax[i].set_ylabel(self.ac.getVarLabel(2*i+1))
            self.ax[i].set_ylim(-g.xlim, g.xlim)
            self.ax[i].set_xlim(-g.ylim, g.ylim)
            
            plotEllipse(mean[-1][np.ix_([2*i, 2*i+1])], cov[-1][np.ix_([2*i, 2*i+1],[2*i, 2*i+1])], self.ax[i], edge = 'red')
            
            self.canvas[i].draw()
        
        # plot 3
        self.ax[3].cla()
        self.ax[3].scatter(sol[:, 0],sol[:, 2],s=s)
        self.ax[3].set_xlabel(self.ac.getVarLabel(0))
        self.ax[3].set_ylabel(self.ac.getVarLabel(2))
        self.ax[3].set_ylim(-g.xlim, g.xlim)
        self.ax[3].set_xlim(-g.ylim, g.ylim)
        
        plotEllipse(mean[-1][np.ix_([0,2])],cov[0][np.ix_([0,2],[0,2])], self.ax[3], edge = 'red')
        
        self.canvas[3].draw()
    
    # ----------------------------------------------------------------------------------
    # Twiss parameters and dispersion plot
    # ----------------------------------------------------------------------------------
    def doTwiss(self):
        
        betax = [beta(zz, self.ac)[0] for zz in self.z]
        betay = [beta(zz, self.ac)[1] for zz in self.z]
        gammax = [gamma(zz, self.ac)[0] for zz in self.z]
        gammay = [gamma(zz, self.ac)[1] for zz in self.z]
        nx = [self.dispersion(zz)[0][0] for zz in self.z]
        
        fig = Figure( figsize=(3 * self.xdim, self.ydim / 2.5), dpi = 100 )
        fig.subplots_adjust(bottom=0.3)
        ax = fig.add_subplot(1,1,1)
        ax.set_xlabel('position')
        
        ax.plot(self.z,np.sqrt(betax),'b',label = r'$\sqrt{\beta_x}$')
        ax.plot(self.z,np.sqrt(gammax),'g',label = r'$\sqrt{\gamma_x}$')
        ax.plot(self.z,np.sqrt(betay),'r',label = r'$\sqrt{\beta_y}$')
        ax.plot(self.z,np.sqrt(gammay),'y',label = r'$\sqrt{\gamma_y}$')
        ax.plot(self.z,nx,'m',label = r'$\eta_x$')
        ax.legend(loc=2, borderaxespad=0., bbox_to_anchor=(1.02, 1), fontsize=12) 
        canvas = FigureCanvasTkAgg(fig, master=self.master)
        canvas.show()
        canvas.get_tk_widget().grid(row=2, columnspan=3)
        
        
        
    # compute the dispersion
    # @param z is the longitudinal position
    def dispersion(self, z):        
        nx0 = np.linalg.inv([[1-self.ac.mats[-1][0,0], -self.ac.mats[-1][1,0]],
                            [-self.ac.mats[-1][0,1], 1 - self.ac.mats[-1][1,1]]]) @ np.array([self.ac.mats[-1][5,0],
                                                                                              self.ac.mats[-1][5,1]])
                        
        ny0 = np.linalg.inv([[1-self.ac.mats[-1][2,2],-self.ac.mats[-1][3,2]],
                            [-self.ac.mats[-1][2,3],1 - self.ac.mats[-1][3,3]]]) @ np.array([self.ac.mats[-1][5,2],
                                                                                             self.ac.mats[-1][5,3]])
        
        mat = self.ac.getMapAtPos(z).T
        
        nx = mat[:2,:2] @ nx0 + mat[:2,5]
        ny = mat[2:4,2:4] @ ny0 + mat[2:4,5]
        
        return nx ,ny
    
    # ----------------------------------------------------------------------------------
    # Line plot of phase space variables (x, px, y, py)
    # ----------------------------------------------------------------------------------
    def doLinePlot(self):
        mean, cov = self.ac.statz(self.z, self.beam)
        
        fig = Figure( figsize=(3 * self.xdim, self.ydim / 3.2), dpi = 100 )
        fig.subplots_adjust(bottom=0.2)
        ax = fig.add_subplot(1,1,1)
        #ax.set_xlabel('position')
        
        ax.plot(self.z, np.sqrt(cov[:,0,0]),'b',label = self.ac.getVarLabel(0))
        ax.plot(self.z, np.sqrt(cov[:,1,1]),'g',label = self.ac.getVarLabel(1))
        ax.plot(self.z, np.sqrt(cov[:,2,2]),'r',label = self.ac.getVarLabel(2))
        ax.plot(self.z, np.sqrt(cov[:,3,3]),'y',label = self.ac.getVarLabel(3))
        ax.legend(loc=2, borderaxespad=0., bbox_to_anchor=(1.02, 1), fontsize=12)
        
        canvas = FigureCanvasTkAgg(fig, master=self.master)
        canvas.show()
        canvas.get_tk_widget().grid(row=3, columnspan=3)
        
    # ----------------------------------------------------------------------------------
    # Plot that shows all beamline elements
    # ----------------------------------------------------------------------------------
    def doBeamline(self):
        fig = Figure( figsize=(3 * self.xdim, self.ydim / 5), dpi = 100 )
        fig.subplots_adjust(bottom=0.6)
        self.pos = fig.add_subplot(1,1,1)
        self.pos.set_xlim(0, self.ac.length * g.nrep)
        self.pos.set_yticks([])
        self.pos.set_ylim(0, self.ydim / 10)
        
        
        self.ac.plot(self.pos, g.nrep)
        self.canv = FigureCanvasTkAgg(fig, master=self.master)
        self.canv.show()
        self.canv.get_tk_widget().grid(row=4, columnspan=3)

    
    # update position plot, called by slider
    # @param z is the longitudinal position
    def doPostionUpdate(self, z):
        self.pos.cla()
        self.ac.plot(self.pos, g.nrep)
        self.pos.set_xlim(0, self.ac.length * g.nrep)
        self.pos.set_yticks([])
        
        # get all element types (unique list) and their index in the list
        types = set()
        for elem in self.ac.elements:
            types.add( (elem.color, elem.getType()) )
        
        # add legend to plot
        handle = []
        for t in types:
            handle.append( patches.Patch(color=t[0], label=t[1]) )
        
        self.pos.legend(handles=handle, loc=1, borderaxespad=0.,
                        bbox_to_anchor=(0., -0.9, 1., 0.1), ncol=len(handle), fontsize=11)
        
        # coloring position in accelerator
        self.pos.fill_betweenx(x1=0, x2=z, y = [0, self.ydim / 7], facecolor='green')
        self.canv.draw()

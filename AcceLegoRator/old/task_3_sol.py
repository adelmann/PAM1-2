# -*- coding: utf-8 -*-
"""

@author: krempel
"""

#import numpy as np  # NumPy (multidimensional arrays, linear algebra, ...)
#import scipy as sp  # SciPy (signal and image processing library)
#import matplotlib as mpl         # Matplotlib (2D/3D plotting library)
#import matplotlib.pyplot as plt  # Matplotlib's pyplot: MATLAB-like syntax
from pylab import *              # Matplotlib's pylab interface
#ion()                            # Turned on Matplotlib's interactive mode
#import guidata  # GUI generation for easy dataset editing and display



electron_mass = 0.51099892e6 # eV/c**2
positron_mass = electron_mass
proton_mass   = 938.272013e6 # eV/c**2
c_light = 299792458 # m/s
e_charge = 1.60217653e-19 # C
Brho1GeV = 1e9 / c_light # T.m / GeV/c


import numpy as np
import matplotlib.pylab as plt
import math
import random
import copy
from itertools import repeat

if ((not 'debugging' in locals()) and (not 'debugging' in globals())):
    debugging=0

if ((not 'GlobalRefMass' in locals()) and (not 'GlobalRefMass' in globals())):
    GlobalRefMass=None
if ((not 'GlobalRefGamma' in locals()) and (not 'GlobalRefGamma' in globals())):
    GlobalRefGamma=None
if ((not 'GlobalRefMomentum' in locals()) and (not 'GlobalRefMomentum' in globals())):
    GlobalRefMomentum=None
if ((not 'GlobalRefEnergy' in locals()) and (not 'GlobalRefEnergy' in globals())):
    GlobalRefEnergy=None
if ((not 'GlobalRefBeta' in locals()) and (not 'GlobalRefBeta' in globals())):
    GlobalRefBeta=None
if ((not 'GlobalRefBeta' in locals()) and (not 'GlobalRefBeta' in globals())):
    GlobalRefCharge=None

class Beam():
    def __init__(self, *args):
        self.particles=[]
        self.AddParticle(0,0,0,0,0,0)
        if len(args) >0:
            if (args[0]=='normal'):
                self.BeamNormal(*args[1:])
            if (args[0]=='ellipse'):
                self.BeamPairwiseHollowEllipse(*args[1:])   
        #todo:
        # ref energy
        # ref gamma
        # ref beta
        
    def AddParticle(self,x,px,y,py,z,pz):
        """add one particle with given parameter to beam"""
        self.particles.append(np.array([x,px,y,py,z,pz],dtype = np.float_))
        #return self  # not really necessary, as Python is call-by-object
    
    def RemoveAllParticles(self):
        self.__init__()
 
    def BeamPairwiseHollowEllipse(self, n, 
                  xr=0 , pxr=0, yr=0 , pyr=0, zr=0, pzr=0,
                  dx=0.1, dy=0.1, dz=0.1,
                  xc=0 , pxc=0, yc=0 , pyc=0, zc=0, pzc=0
                              ):
        """Create a Beam (list of n Particles) in pairwise ellipsoid shape
            we use poor man random number selection that makes it extremely slow
            furthermore it looks thinner on major axis, ==> most likely buggy"""
        def RandomEllipse(a,b,d):
            radius= max( a,b)
            Sum=-2*d
            while Sum > (1+d/2)**2 or Sum < (1-d/2)**2 :   #Dice until point is inside ellipse
                Sum=0
                x1 = (random.random()*2-1) * radius
                x2 = (random.random()*2-1) * radius 
                Sum += x1*x1 / (a*a)
                Sum += x2*x2 / (b*b)
            return (x1,x2)
        for i in range(n):
            x,px = RandomEllipse(xr,pxr,dx)
            y,py = RandomEllipse(yr,pyr,dy)
            z,pz = RandomEllipse(zr,pzr,dz)
            self.AddParticle(
                      xc-x    ,       pxc-px  ,
                      yc-y    ,       pyc-py  ,
                      zc-z    ,       pzc-pz   )
                
    def BeamNormal(self, n, xr=0 , pxr=0, yr=0 , pyr=0, zr=0, pzr=0,
                  xc=0 , pxc=0, yc=0 , pyc=0, zc=0, pzc=0):
        """Create a Beam (list of n Particles) with Gaussian distribution 
             width and center can be specified in all dimension
             and each is independend from all other"""
        for i in range(n):
            self.AddParticle(
              xc  + (np.random.normal(0,xr)  if xr  != 0 else 0),
              pxc + (np.random.normal(0,pxr) if pxr != 0 else 0),
              yc  + (np.random.normal(0,yr)  if yr  != 0 else 0),
              pyc + (np.random.normal(0,pyr) if pyr != 0 else 0),
              zc  + (np.random.normal(0,zr)  if zr  != 0 else 0),
              pzc + (np.random.normal(0,pzr) if pzr != 0 else 0)  ) 
        #return Beam    
        
    def PlotTriple(self,num=None):
        plt.figure(num=num)
        plt.subplot(2,2,1)
        self.PlotXPx(style='ro')
        plt.subplot(2,2,2)
        self.PlotYPy(style='go')
        plt.subplot(2,2,3)
        self.PlotZPz(style='bo')
        
    def PlotXPx(self,style=None):   
        self.Plot1DPhaseSpace(0, style=style)
    def PlotYPy(self, **kwargs):   # same as "style=None" above, but be flexible for future extensions
        self.Plot1DPhaseSpace(1, **kwargs)
    def PlotZPz(self, **kwargs):   
        self.Plot1DPhaseSpace(2, **kwargs)
        
    def Plot1DPhaseSpace(self,dimension,style=None):
        xdim=dimension*2
        ydim=dimension*2+1
        myx=[]
        mypx=[]  
        for parti in self.particles:
            myx.append(parti[xdim])
            mypx.append(parti[ydim])
        if (style is None):
            style='go'
        plt.plot(myx,mypx,style)

    def CalcEmittance(self, debug=debugging):
        center = np.array([0,0,0,0,0,0],dtype = np.float_)
        for p in self.particles:
            center += p
        center/=len(self.particles)
        Mcore = np.matrix([[0,0,0,0,0,0],
                           [0,0,0,0,0,0],
                           [0,0,0,0,0,0],
                           [0,0,0,0,0,0],
                           [0,0,0,0,0,0],
                           [0,0,0,0,0,0]],dtype = np.float_)
        for p in self.particles:
            Mcore += np.dot(np.matrix(p-center).transpose(),  (np.matrix(p-center)))
        Mcore/=len(self.particles)
        if debug != 0:
            print Mcore
        sigma = math.pi*math.sqrt(np.linalg.det(Mcore))
        return sigma


            
class LineElement(object):
    """The Element base class.  All beamline element classes should
    inherit from this class"""
    Length=0
    Map=None
    q=None
    def __init__(self, name=None, L=1, S=-1, Q=None):
        self.name = name                                         # Element name
        self.Length = L                                          # Element length
        self.S = S                                               # S position of start of element
        if not Q is None:
            self.q=Q
        self.offset = np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0])   # (x,px,y,py,z,P)
        self.x = np.zeros(6)
     #   self.twiss = Twiss()
        # if self.Map is None:
            # self.Map=np.array([
                # [1, 0, 0, 0, 0, 0],
                # [0, 1, 0, 0, 0, 0],
                # [0, 0, 1, 0, 0, 0],
                # [0, 0, 0, 1, 0, 0],
                # [0, 0, 0, 0, 1, 0],
                # [0, 0, 0, 0, 0, 1],
                # ])
        #self.updateMap()
    def getMap(self):
        if self.Map is None:
            self.updateMap()
        return self.Map
        
    def updateRefValues(self):
        if ('GlobalRefMass' in globals()) and ( not GlobalRefMass is None) and (GlobalRefMass > 0) :
            self.RefMass = GlobalRefMass        
        else:
            self.RefMass = proton_mass        
        #get Design velocity (aka Reference velocity from a global Variable
        #this is not very elegant.
        #the question is: when to do this? 
        #  once at initialization of element, or each time before updateMap?)
        if  (not GlobalRefGamma is None) and (GlobalRefGamma >0):         #DesignGamma factor
            self.RefGamma=GlobalRefGamma
            self.RefBeta=math.sqrt(1-1/self.RefGamma**2)
            self.P = self.RefGamma* c_light  * self.RefBeta * self.RefMass         
        elif (not GlobalRefMomentum is None) and (GlobalRefMomentum >0):    # Design momentum 
            self.P = GlobalRefMomentum
            self.RefGamma=math.sqrt((self.P/self.RefMass/c_light)**2+1)
            self.RefBeta=math.sqrt(1-1/self.RefGamma**2)
        elif (not GlobalRefEnergy is None) and (GlobalRefEnergy >0):      # DesignEnergy
            self.RefGamma=GlobalRefEnergy / self.RefMass / c_light**2
            self.RefBeta=math.sqrt(1-1/self.RefGamma**2)
            self.P = self.RefGamma* c_light  * self.RefBeta * self.RefMass        
        elif (not GlobalRefBeta is None) and (GlobalRefBeta >0):              #design velocity
            self.RefBeta=GlobalRefBeta
            self.RefGamma=1/math.sqrt(1-self.RefBeta**2)
            self.P = self.RefGamma* c_light  * self.RefBeta * self.RefMass         
        else:
            raise NameError("No design Energy,Momentum, or velocity specified.")
        if self.q is None:
            if not GlobalRefCharge is None:
                self.q=GlobalRefCharge
            else:
                self.q=e_charge
                print "WARNING: in Magnet, assume charge = +1e"

            
    def updateMap(self):
        raise NameError('Each element must provide its own update function')
        
    def TrackThroughElement(self,Beam):
        """track a full beam through a single element
        particle list of beam will be overwritten"""
        if self.Map is None:
            self.updateMap()
        newparticles=[]
        for item in Beam.particles:
            item = np.dot(self.getMap(),item)
            newparticles.append(item)
        Beam.particles=newparticles        


class DriftSpace(LineElement):
    def __init__(self, L=0, **kwargs):
        #self.Length=Length
        LineElement.__init__(self, L=L, **kwargs)

    def updateMap(self):
        self.updateRefValues()
        self.Map=np.array([
            [1, self.Length, 0, 0, 0, 0],
            [0, 1, 0, 0, 0, 0],
            [0, 0, 1, self.Length, 0, 0],
            [0, 0, 0, 1, 0, 0],
            [0, 0, 0, 0, 1, self.Length/((self.RefBeta*self.RefGamma)**2)],
            [0, 0, 0, 0, 0, 1],
            ])


class BasicMagnet(LineElement):
    """A class describing the operation of magnets.  Inherits from Element."""
    def __init__(self,  B=0, tilt=0, **kwargs):
        self.B = B
        self.tilt = tilt
        LineElement.__init__(self, **kwargs)

    def SetTilt(self, tilt) :
        """Sets the tilt of the element"""
        self.tilt = tilt

    def GetTilt(self) :
        """Returns the tilt of the element"""
        return self.tilt
    
    #def TrackThruEle(self, beam_in):
     #   pass
 
class Dipole(BasicMagnet):
    #h = 1.6021773e-10  #h = 1 + kx + ky... curvature obligatorily given in GeV^-1. 1/1m = 1.6021773e-10GeV^-1.
    #h=0.1
    #todo: probably better to have omega outside, and various possibility to define it.
    #  needs then its own update function
    #def __init__(self, name=None, L=1, P=1, S=0, B=0, tilt=0):
    def __init__(self, B=None, r0=None, h=None, k=None, **kwargs):
        self.focusPriority =[]
        if (not B is None):
            self.B = B
            self.focusPriority.append("B")
        if (not r0 is None):
            self.r0 = r0
            self.focusPriority.append("r0")
        if (not h is None):
            self.h = h
            self.focusPriority.append("h")
        if (not k is None):
            self.k = k
            self.focusPriority.append("k")
        BasicMagnet.__init__(self, B=B, **kwargs)
    def SetB(self, B) :
        """Sets the B field of the element"""
        self.B = B
        self.focusPriority.append("B")
    def GetB(self) :
        """Returns the B field of the element"""
        return self.B
    def update(self):
        self.CalcOmega()
        omega = self.omega
        COS = math.cos(omega*self.Length)
        SIN = math.sin(omega*self.Length)
        self.Map = np.array([
                    [COS,             SIN/omega,    0,          0,          0,         (1-COS)/(omega*self.RefBeta)],
                    [-omega*SIN,      COS,          0,          0,          0,         SIN/self.RefBeta],
                    [0,             0,          1,          self.Length,0,         0],
                    [0,             0,          0,          1,          0,         0],
                    [-SIN/self.RefBeta, -(1-COS)/(omega*self.RefBeta),0,0,      1,         1/((self.RefBeta*self.RefGamma)**2)-(omega*self.Length-SIN)/(omega*self.RefBeta*self.RefBeta)],
                    [0,             0,          0,          0,          0,         1]
                    ])
    updateMap=update
    def CalcOmega(self):
        self.updateRefValues()
        if "B" in self.focusPriority:
            self.k= self.q / self.P * self.B
        if "r0" in self.focusPriority:   
            self.h= 1 / self.r0
        if not ("B" in self.focusPriority or "k" in self.focusPriority):
            self.omega = self.h     #We assume a matched dipole
        elif not ("h" in self.focusPriority or "r0" in self.focusPriority):
            self.omega = self.k     #We assume a matched dipole
        elif ("B" in self.focusPriority or "k" in self.focusPriority) and ("h" in self.focusPriority or "r0" in self.focusPriority):
            self.omega = np.sqrt(abs( self.h * self.k))  
        else:
            raise NameError("Dipole, no information how to calculate curvature")
        self.signB = np.sign(self.k)    



class Quadrupole(BasicMagnet):
    """Quad class, do not use directly, for inheritance only"""
    def CalcRmat(self, P=-1):
        """outdated,  not used any more:
        Calculate the R matrix of the element
        We assume an upright (non-skew) magnet.
        The skew quadrupole class will overwrite this function"""
        if P == -1:
            P = self.P
        L = self.Length
        if self.B == 0:
             self.Map=DriftSpace(self.Length).getMap()
             #self.R = DriftRmat(L)
        else:
        #following script D.4    
            signB=np.sign(self.B);
            Brho = Brho1GeV * P
            omega = np.sqrt(SignB * (self.B / self.Length ) / Brho)
        #k1=q / P0 *b2 / r0
        #omega = np.sqrt(k1)
        COS = math.cos(omega*self.Length)
        SIN = math.sin(omega*self.Length)
        COSHYP = math.cosh(omega*self.Length)
        SINHYP = math.sinh(omega*self.Length)
        self.Map = np.array([
                    [COS,              SIN/omega, 0,              0,            0,  0],
                    [-signB*omega*SIN, COS,       0,              0,            0,  0],
                    [0,                0,         COSHYP,         SINHYP/omega, 0,  0],
                    [0,                0,         omega * SINHYP, COSHYP,       0,  0],
                    [0,                0,         0,              0,            1,  self.Length/((self.RefBeta*self.RefGamma)**2)],
                    [0,                0,         0,              0,            0,  1]
                    ])        
    #updateMap=CalcRmat
    
class QuadrupoleUpright(Quadrupole):
    """Upright (non-skew) Quadrupoles, 
    This should not be called directly, but the inheriting classes"""
    def updateMap(self):
        """Calculate the R matrix of the element
        for an upright (non-skew) magnet."""
        self.updateRefValues()
        self.CalcOmega()
        omega=self.omega
        signB=self.signB
        #following script D.4    
        #    signB=np.sign(self.B);
        #    Brho = Brho1GeV * P
        #    omega = np.sqrt(SignB * (self.B / self.Length ) / Brho)
        #k1=q / P0 *b2 / r0
        #omega = np.sqrt(k1)
        COS = math.cos(omega*self.Length)
        SIN = math.sin(omega*self.Length)
        COSHYP = math.cosh(omega*self.Length)
        SINHYP = math.sinh(omega*self.Length)
        if signB > 0:
            self.Map = np.array([
                    [COS,          SIN/omega, 0,              0,            0,  0],
                    [-omega*SIN,   COS,       0,              0,            0,  0],
                    [0,            0,         COSHYP,         SINHYP/omega, 0,  0],
                    [0,            0,         omega * SINHYP, COSHYP,       0,  0],
                    [0,            0,         0,              0,            1,  self.Length/((self.RefBeta*self.RefGamma)**2)],
                    [0,            0,         0,              0,            0,  1]
                    ])   
        else:
            self.Map = np.array([
                    [COSHYP,         SINHYP/omega, 0,           0,          0,  0],
                    [omega * SINHYP, COSHYP,       0,           0,          0,  0],
                    [0,                0,          COS,         SIN/omega,  0,  0],
                    [0,                0,         -omega*SIN,   COS,        0,  0],
                    [0,                0,         0,            0,          1,  self.Length/((self.RefBeta*self.RefGamma)**2)],
                    [0,                0,         0,            0,          0,  1]
                    ])   
            
    def CalcOmega(self):
        raise NameError("Error in QuadrupoleUpright.CalcOmega(). Don't know how to calc omega. You must use sub-class!")

class QuadrupoleSkew(Quadrupole):
    """Skew Quadrupoles, 
    This should not be called directly, but the inheriting classes"""
    def updateMap(self):
        """Calculate the R matrix of the element
        for a skew magnet."""
        self.updateRefValues()
        self.CalcOmega()
        omega=self.omega
        signB=self.signB
        #following script D.4    
        #    signB=np.sign(self.B);
        #    Brho = Brho1GeV * P
        #    omega = np.sqrt(SignB * (self.B / self.Length ) / Brho)
        #k1=q / P0 *b2 / r0
        #omega = np.sqrt(k1)
        COS = math.cos(omega*self.Length)
        SIN = math.sin(omega*self.Length)
        COSHYP = math.cosh(omega*self.Length)
        SINHYP = math.sinh(omega*self.Length)
        if signB > 0:
            self.Map = 0.5 * np.array([
                    [COS+COSHYP,               (SIN +SINHYP)/omega,  COS - COSHYP,         (SIN -SINHYP)/omega,   0, 0 ],
                    [-(SIN-SINHYP)*omega,      COS+COSHYP,           (SIN +SINHYP)*omega,   COS - COSHYP,         0, 0],
                    [COS - COSHYP,             (SIN -SINHYP)/omega,  COS + COSHYP,         (SIN +SINHYP)/omega,   0, 0 ],
                    [-(SIN + SINHYP)*omega,    COS - COSHYP,         (SIN -SINHYP)*omega,   COS + COSHYP,         0, 0 ],
                    [0,                        0,                    0,                    0,                     2,  2* self.Length/((self.RefBeta*self.RefGamma)**2)],
                    [0,                        0,                    0,                    0,                     0,  2]
                    ])   
        else:
            raise NameError("Error: Skew Quadrupole with negative moment not implemented")
    def CalcOmega(self):
        raise NameError("Error in QuadrupoleUpright.CalcOmega(). Don't know how to calc omega. You must use sub-class!")
        

class QuadrupoleUprightMoments(QuadrupoleUpright):
    """Upright (non-skew) Quadrupoles, 
    in case moments of B-field are provided to define the magnet"""
    def __init__ (self, L, b2, r0, **kwargs):
        self.b2=b2
        self.r0=r0
        self.Length=L
        BasicMagnet.__init__(self, L=L, **kwargs)
    def CalcOmega(self):
        k1=self.q / self.P *self.b2 / self.r0 
        self.omega = np.sqrt(abs(k1))
        self.signB = np.sign(k1)            
        
class QuadrupoleUprightGrad(QuadrupoleUpright):
    def __init__ (self, L, k, **kwargs):
        self.k=k
        self.Length=L
        BasicMagnet.__init__(self, L=L, **kwargs)
    def CalcOmega(self):
        self.omega = np.sqrt(abs(self.k))   
        self.signB = np.sign(self.k)        
        
class QuadrupoleSkewMoments(QuadrupoleSkew):
    """Upright (non-skew) Quadrupoles, 
    in case moments of B-field are provided to define the magnet"""
    def __init__ (self, L, a2, r0, **kwargs):
        self.a2=a2
        self.r0=r0
        self.Length=L
        BasicMagnet.__init__(self, L=L, **kwargs)
    def CalcOmega(self):
        k1=self.q / self.P *self.a2 / self.r0 
        self.omega = np.sqrt(abs(k1))
        self.signB = np.sign(k1)            
        
class QuadrupoleSkewGrad(QuadrupoleSkew):
    def __init__ (self, L, k, **kwargs):
        self.k=k
        self.Length=L
        BasicMagnet.__init__(self, L=L, **kwargs)
    def CalcOmega(self):
        self.omega = np.sqrt(abs(self.k))   
        self.signB = np.sign(self.k)        
       
    
    
class QuadrupoleThin(BasicMagnet):
    """Quad class"""
    def __init__ (self, L=0, f=None, **kwargs):
        self.f=f
        self.Length=L
        BasicMagnet.__init__(self, L=L, **kwargs)    
    def updateMap(self):
        """Calculate the R matrix of the element"""
        #self.updateRefValues()
        if self.f is None or self.f==0:
            raise  NameError('Thin Quadrupole without focal length')
        self.Map = np.array([
                [1,         0,    0,         0,  0,  0],
                [-1/self.f, 1,    0,         0,  0,  0],
                [0,         0,    1,         0,  0,  0],
                [0,         0,    1/self.f,  1,  0,  0],
                [0,         0,    0,         0,  1,  0],
                [0,         0,    0,         0,  0,  1]
                ])        

    
class Line(LineElement,list):

    def __mul__(self, fact):
        """Allows multiplication of a small lattice subset by an integer in 
        order to easily define a repeated section"""
        new_line = Line()
        copyfunc = lambda x: new_line.extend(copy.deepcopy(x))
        for rep in repeat(copyfunc, fact):
            rep(self)
        return new_line

    def __repr__(self):
        def namecatch(inst):
            try: return str(inst.name)
            except AttributeError: return "No name attr"

        ret = '\n'.join(namecatch(ele)+" :: "+str(ele.__class__) for ele in self)
        return ret       
    def updateMap(self):
        for myElement in self:
            myElement.updateMap()
        
        
        
# A usage example
        # runfile('task_3_sol.py')
        
if __name__ == '__main__':
    pass

if 1==1:
    GlobalRefGamma=50
    GlobalRefMass=proton_mass
    GlobalRefCharge=e_charge
    
    myBeam=Beam('normal',400,1.1,1.11,1.2,1.21,1.3,1.31)
#   my2Beam=Beam('ellipse',40,1.1,1.11,1.2,1.21,1.3,1.31,0.1,0.2,0.3)
    myBeam.PlotTriple(0)
    print "DriftSpace:"
    myele=DriftSpace(L=10)
    myele.getMap()
    print "Dipole:"
    mydip=Dipole(B=1)
#    mydip.update()
    mydip.getMap()
    print "Quadrupole:"
    myquad=QuadrupoleUprightMoments(L=1, b2=0.3, r0=40)
    myquad.getMap()
    
    myele.TrackThroughElement(myBeam)
    myquad.TrackThroughElement(myBeam)
#    mydip.TrackThroughElement(myBeam)
    myBeam.PlotTriple(1)

#    Shortline = Line()
#    Shortline.append(Drift(name='ele1', L=0.75))
#    Shortline.append(Quad(name='ele2', L=0.25, B=5))
#    Shortline.append(Drift(name='ele3', L=1))
#    Shortline.append(Quad(name='ele4', L=0.25, B=-5))
#    beamline = Shortline * 5        
    
    if not matplotlib.is_interactive():
        print "You can have now a look on the plots,"
        print "and e.g., use the zooming features."
        print "once you are finished, close all plot-windows to continue this script"
    plt.show()

print " task_3_sol.py finished"    
import numpy as np
import matplotlib.pylab as plt

#stolen from serpentine utilities.py
def DriftRmat(L):
    """Return the R matrix for a drift"""
    R = np.array([
        [1, L, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0],
        [0, 0, 1, L, 0, 0],
        [0, 0, 0, 1, 0, 0],
        [0, 0, 0, 0, 1, 0],
        [0, 0, 0, 0, 0, 1],
        ])
    return R
	
#initial single Particle	
#inPart=[0, 0.2, 0, 0.2, 0, 0]	

inPart=np.array([0, 0.2, 0, 0.2, 0, 0])

Beam = [ inPart, 2*inPart, 2.1* inPart]

outPart = np.dot(DriftRmat(7),inPart)

OutBeam=[];
for parti in Beam:
	OutBeam.append( np.dot(DriftRmat(7),parti) )

print inPart
print outPart

print "\n"

print OutBeam

print (OutBeam[:])[2]
print (OutBeam[1])[:]
  #this indexing is strange
  
x=[]
px=[]  
for parti in OutBeam:
   x.append(parti[0])
   px.append(parti[1])
   
print x
print px   

plt.plot(x,px,'ro')

print "ende"	
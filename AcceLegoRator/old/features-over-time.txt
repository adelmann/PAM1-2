
sol 4.7  task 3   Drift space, quadrupoles, dipoles
    track a beam through sequence
    option create them from multipole

sol 6.5   task 5   FODO Cells

  FODO (f_0, L)
    track single particle  off center  (differnt starting x,y)
    normal distributed beams

sol 7.7b (taks 6)  Synchrotron    ring with 28 Dipole magnets
    dipoles (B-field)
    multiple equal FODO ()
    matching???

sol 8.1  betatron tunes
    FODO  
       Thin lens (suitable strength for stability)
       sector magnet (dipole)
    slicing
    beta_x, beta_y, eta  plot vs z
    matching

sol 8.2  betatron tunes  beta_x beta_y eta (splicing)

sol 10.3  linear space charge
  FODO
  matching
  splice
  rms beam size
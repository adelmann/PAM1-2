import numpy as np
import matplotlib.pylab as plt
import math
import random

#Solution to Task 1
# simple version = without class but functional style

#stolen from serpentine utilities.py
print "start script now"

def DriftRmat(L):
    """Return the R matrix for a drift"""
    R = np.array([
        [1, L, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0],
        [0, 0, 1, L, 0, 0],
        [0, 0, 0, 1, 0, 0],
        [0, 0, 0, 0, 1, 0],
        [0, 0, 0, 0, 0, 1],
        ])
    return R
	
#initial single Particle	
#inPart=[0, 0.2, 0, 0.2, 0, 0]	
 
def AddParticle(Beam,x,px,y,py,dz,pz):
    """add particle with given given parameter to beam"""
    Beam.append(np.array([x,px,y,py,dz,pz]))
    return Beam  # not really necessary, as Python is call-by-object

#def BeamNormal(n, dx,dpx, dy,dpy, dz,dpz):
def BeamNormal(n, xr=0 , pxr=0, yr=0 , pyr=0, zr=0, pzr=0,
                  xc=0 , pxc=0, yc=0 , pyc=0, zc=0, pzc=0):
    """Create a Beam (list of n Particles) with Gaussian distribution 
         width and center can be specified in all dimension
         and each is independend from all other"""
    Beam=[]
    for i in range(n):
        AddParticle(Beam,
          xc  + (np.random.normal(0,xr)  if xr  != 0 else 0),
          pxc + (np.random.normal(0,pxr) if pxr != 0 else 0),
          yc  + (np.random.normal(0,yr)  if yr  != 0 else 0),
          pyc + (np.random.normal(0,pyr) if pyr != 0 else 0),
          zc  + (np.random.normal(0,zr)  if zr  != 0 else 0),
          pzc + (np.random.normal(0,pzr) if pzr != 0 else 0)  ) 
    return Beam

def BeamSpotEllipse(n, xr=0 , pxr=0, yr=0 , pyr=0, zr=0, pzr=0,
                       xc=0 , pxc=0, yc=0 , pyc=0, zc=0, pzc=0):
    """Create a Beam (list of n Particles) in ellipsoid shape
      with uniform density
      we use poor man random number selection"""
    Beam=[]
    for i in range(n):
        #Ellipse equation is  (x/a)^2 + (y/b)^2 ==1
        # we extend to 6 dimension
        # for performence we test with random number -1..+1 before scaling them
        Sum=2
        #while Sum > 1.1 or Sum < 0.9 :   #Dice until point is inside ellipse
        while Sum > 1 :   #Dice until point is inside ellipse
            x = (random.random()*2-1)
            px= (random.random()*2-1)
            y = (random.random()*2-1)
            py= (random.random()*2-1)
            z = (random.random()*2-1)
            pz= (random.random()*2-1)
            Sum = 0
            if  xr != 0:
                Sum += x*x
            if pxr != 0:
                Sum += px*px
            if  yr != 0:
                Sum +=  y* y 
            if pyr != 0:
                Sum += py*py
            if  zr != 0:
                Sum +=  z* z 
            if pzr != 0:
                Sum += pz*pz 
        AddParticle(Beam,
          xc-xr   * x  ,
          pxc-pxr * px ,
          yc-yr   * y  ,
          pyc-pyr * py ,
          zc-zr   * z  ,
          pzc-pzr * pz   )
    return Beam



def BeamSpotPseudoEllipse(n, xr=0 , pxr=0, yr=0 , pyr=0, zr=0, pzr=0,
                       xc=0 , pxc=0, yc=0 , pyc=0, zc=0, pzc=0):
    """Create a Beam (list of n Particles) 
      tried to have solid uniform ellipse
      however with independed dimensions this is difficult in efficient random numbers.
      Thus result is somethat triangular decaying"""
    Beam=[]
    for i in range(n):
        AddParticle(Beam,
          xc-xr   * math.sqrt(random.random()) * (random.random() -0.5 ) *2,
          pxc-pxr * math.sqrt(random.random()) * (random.random() -0.5 ) *2,
          yc-yr   * math.sqrt(random.random()) * (random.random() -0.5 ) *2,
          pyc-pyr * math.sqrt(random.random()) * (random.random() -0.5 ) *2,
          zc-zr   * math.sqrt(random.random()) * (random.random() -0.5 ) *2,
          pzc-pzr * math.sqrt(random.random()) * (random.random() -0.5 ) *2  )
# stealing from here http://www.anderswallin.net/2009/05/uniform-random-points-in-a-circle-using-polar-coordinates/
#but with uncorelated dimensionsthis does not work out        
    return Beam

def BeamSpotBlock(n, xr=0 , pxr=0, yr=0 , pyr=0, zr=0, pzr=0,
                       xc=0 , pxc=0, yc=0 , pyc=0, zc=0, pzc=0):
    """Create a Beam (list of n Particles) with uniform density
       width and offset can be specified in all dimensions"""
    Beam=[]
    for i in range(n):
        AddParticle(Beam,
          xc-xr   *  (random.random() -0.5 ) *2,
          pxc-pxr *  (random.random() -0.5 ) *2,
          yc-yr   *  (random.random() -0.5 ) *2,
          pyc-pyr *  (random.random() -0.5 ) *2,
          zc-zr   *  (random.random() -0.5 ) *2,
          pzc-pzr *  (random.random() -0.5 ) *2  )
    return Beam




def PlotxPx(myBeam):
    myx=[]
    mypx=[]  
    for parti in myBeam:
        myx.append(parti[0])
        mypx.append(parti[1])
    plt.clf()
    plt.plot(myx,mypx,'ro')

def PlotyPy(myBeam):
    myx=[]
    myy=[]  
    for parti in myBeam:
        myx.append(parti[2])
        myy.append(parti[3])
    plt.clf()
    plt.plot(myx,myy,'ro')
    
def PlotxPxHist(myBeam, bins=None):
    myx=[]
    mypx=[]  
    if bins is None:
        bins=math.sqrt(len(myBeam))/10
    for parti in myBeam:
        myx.append(parti[0])
        mypx.append(parti[1])
    plt.clf()
    plt.hist2d(myx,mypx,bins=bins)    

    
Beam = BeamNormal(10000, 2 , 3, 1.4 ,0, 0,0)
Beam = BeamSpotPseudoEllipse(10000,xc=-2,xr=4,pxc=9,pxr=1)
Beam = BeamSpotEllipse(10000,xr=4,pxr=1)
Beam = BeamSpotBlock(10000,xc=-2,xr=4,pxc=9,pxr=1)
#AddParticle(Beam,1.1,1.2, 2.4,2.5, 3.6,3.7)    
#AddParticle(Beam,2.1,2.2, 4.4,5.5, 8.6,8.7)    
#AddParticle(Beam,3.1,3.2, 5.4,4.5, 9.6,9.7)    

import time
PlotxPx(Beam)
plt.draw()
print "graph 1 is plotted now"
plt.pause(5)  # wait 5s and view graph

#PlotyPy(Beam)
PlotxPxHist(Beam,20)
print "graph 2 is plotted now"    
        
        
    


print "end script now"



'''

return

inPart=np.array([0, 0.2, 0, 0.2, 0, 0])

Beam = [ inPart, 2*inPart, 2.1* inPart]

outPart = np.dot(DriftRmat(7),inPart)

OutBeam=[];
for parti in Beam:
	OutBeam.append( np.dot(DriftRmat(7),parti) )

print inPart
print outPart

print "\n"

print OutBeam

print (OutBeam[:])[2]
print (OutBeam[1])[:]
  #this indexing is strange
  
x=[]
px=[]  
for parti in OutBeam:
   x.append(parti[0])
   px.append(parti[1])
   
print x
print px   

plt.plot(x,px,'ro')

print "ende"	
'''

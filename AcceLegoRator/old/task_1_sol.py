import numpy as np
import matplotlib.pylab as plt
import math
import random

#Solution to Task 1
# simple version = without class but functional style

print "start script now"

def AddParticle(Beam,x,px,y,py,dz,pz):
    """add one particle with given parameter to beam"""
    Beam.append(np.array([x,px,y,py,dz,pz]))
    return Beam  # not really necessary, as Python is call-by-object

def BeamNormal(n, xr=0 , pxr=0, yr=0 , pyr=0, zr=0, pzr=0,
                  xc=0 , pxc=0, yc=0 , pyc=0, zc=0, pzc=0):
    """Create a Beam (list of n Particles) with Gaussian distribution 
         width and center can be specified in all dimension
         and each is independend from all other"""
    Beam=[]
    for i in range(n):
        AddParticle(Beam,
          xc  + (np.random.normal(0,xr)  if xr  != 0 else 0),
          pxc + (np.random.normal(0,pxr) if pxr != 0 else 0),
          yc  + (np.random.normal(0,yr)  if yr  != 0 else 0),
          pyc + (np.random.normal(0,pyr) if pyr != 0 else 0),
          zc  + (np.random.normal(0,zr)  if zr  != 0 else 0),
          pzc + (np.random.normal(0,pzr) if pzr != 0 else 0)  ) 
    return Beam

def BeamSpotBlock(n, xr=0 , pxr=0, yr=0 , pyr=0, zr=0, pzr=0,
                       xc=0 , pxc=0, yc=0 , pyc=0, zc=0, pzc=0):
    """Create a Beam (list of n Particles) with uniform density
       width and offset can be specified in all dimensions"""
    Beam=[]
    for i in range(n):
        AddParticle(Beam,
          xc-xr   *  (random.random() -0.5 ) *2,
          pxc-pxr *  (random.random() -0.5 ) *2,
          yc-yr   *  (random.random() -0.5 ) *2,
          pyc-pyr *  (random.random() -0.5 ) *2,
          zc-zr   *  (random.random() -0.5 ) *2,
          pzc-pzr *  (random.random() -0.5 ) *2  )
    return Beam
    
def BeamSpotEllipseBad(n, xr=0 , pxr=0, yr=0 , pyr=0, zr=0, pzr=0,
                       xc=0 , pxc=0, yc=0 , pyc=0, zc=0, pzc=0):
    """Create a Beam (list of n Particles) in ellipsoid shape
      with uniform density
      we use poor man random number selection
      Little Bug: this one is uniform, but not isotropic"""
    Beam=[]
    for i in range(n):
        #Ellipse equation is  (x/a)^2 + (y/b)^2 ==1
        # we extend to 6 dimension
        # for performence we test with random number -1..+1 before scaling them
        Sum=2
        #while Sum > 1.1 or Sum < 0.9 :   #Dice until point is inside ellipse
        while Sum > 1 :   #Dice until point is inside ellipse
            x = (random.random()*2-1)
            px= (random.random()*2-1)
            y = (random.random()*2-1)
            py= (random.random()*2-1)
            z = (random.random()*2-1)
            pz= (random.random()*2-1)
            Sum = 0
            if  xr != 0:
                Sum += x*x
            if pxr != 0:
                Sum += px*px
            if  yr != 0:
                Sum +=  y* y 
            if pyr != 0:
                Sum += py*py
            if  zr != 0:
                Sum +=  z* z 
            if pzr != 0:
                Sum += pz*pz 
        AddParticle(Beam,
          xc-xr   * x  ,
          pxc-pxr * px ,
          yc-yr   * y  ,
          pyc-pyr * py ,
          zc-zr   * z  ,
          pzc-pzr * pz   )
    return Beam

def BeamSpotEllipse(n, xr=0 , pxr=0, yr=0 , pyr=0, zr=0, pzr=0,
                       xc=0 , pxc=0, yc=0 , pyc=0, zc=0, pzc=0):
    """Create a Beam (list of n Particles) in ellipsoid shape
      with uniform density
      we use poor man random number selection"""
    Beam=[]
    radius= max( xr, pxr, yr, pyr, zr, pzr)
    #we need to scale the random number with the radius
    #important to get an isotropic distribution: this scaling mus be equal in all directions
    for i in range(n):
        #Ellipse equation is  (x/a)^2 + (y/b)^2 ==1
        # we extend to 6 dimension
        # for performence we test with random number -1..+1 before scaling them
        Sum=2
        #while Sum > 1.1 or Sum < 0.9 :   #Dice until point is inside ellipse
        while Sum > 1 :   #Dice until point is inside ellipse
            x = (random.random()*2-1) * radius if  xr != 0 else 0.0
            px= (random.random()*2-1) * radius if pxr != 0 else 0.0
            y = (random.random()*2-1) * radius if  yr != 0 else 0.0
            py= (random.random()*2-1) * radius if pyr != 0 else 0.0
            z = (random.random()*2-1) * radius if  zr != 0 else 0.0
            pz= (random.random()*2-1) * radius if pzr != 0 else 0.0
            Sum = 0.0
            if  xr != 0:
                Sum += x*x / (xr*xr)
            if pxr != 0:
                Sum += px*px / (pxr*pxr)
            if  yr != 0:
                Sum +=  y* y / (yr*yr)
            if pyr != 0:
                Sum += py*py / (pyr*pyr)
            if  zr != 0:
                Sum +=  z* z / (zr*zr)
            if pzr != 0:
                Sum += pz*pz / (pzr*pzr)
        AddParticle(Beam,
          xc-x    ,
          pxc-px  ,
          yc-y    ,
          pyc-py  ,
          zc-z    ,
          pzc-pz   )
    return Beam



def PlotxPx(myBeam):
    myx=[]
    mypx=[]  
    for parti in myBeam:
        myx.append(parti[0])
        mypx.append(parti[1])
    plt.clf()
    plt.plot(myx,mypx,'ro')

def PlotyPy(myBeam):
    myx=[]
    myy=[]  
    for parti in myBeam:
        myx.append(parti[2])
        myy.append(parti[3])
    plt.clf()
    plt.plot(myx,myy,'ro')
    
def PlotxPxHist(myBeam, bins=None):
    myx=[]
    mypx=[]  
    if bins is None:
        bins=math.sqrt(len(myBeam))/10
    for parti in myBeam:
        myx.append(parti[0])
        mypx.append(parti[1])
    plt.clf()
    plt.hist2d(myx,mypx,bins=bins)    

print "done"

if 1==1:
    
    Beam=[]
    
    AddParticle(Beam,0.8,0.8, 2.4,2.5, 3.6,3.7) 
    AddParticle(Beam,-0.8,0.8, 2.4,2.5, 3.6,3.7) 
    AddParticle(Beam,-1.1,-0.45, 2.4,2.5, 3.6,3.7) 
    AddParticle(Beam,1.1,-0.45, 2.4,2.5, 3.6,3.7) 
    AddParticle(Beam,-0.6,-0.55, 2.4,2.5, 3.6,3.7) 
    AddParticle(Beam,0.6,-0.55, 2.4,2.5, 3.6,3.7) 
    AddParticle(Beam,0,-0.6, 2.4,2.5, 3.6,3.7) 
    AddParticle(Beam,0,0, 2.4,2.5, 3.6,3.7) 
    AddParticle(Beam,0,0.1, 2.4,2.5, 3.6,3.7) 
    AddParticle(Beam,0,0.2, 2.4,2.5, 3.6,3.7) 
    
    PlotxPx(Beam)
    print "graph 1 is plotted now"
    plt.pause(5)  # wait 5s and view graph
        
    Beam = BeamNormal(10000, 2 , 3, 1.4 ,0, 0,0)
    PlotxPxHist(Beam,20)
    print "graph 2a is plotted now"
    plt.pause(5)  # wait 5s and view graph
    PlotxPx(Beam)
    print "graph 2b is plotted now"
    plt.pause(5)  # wait 5s and view graph
    
if 1==1:    
    Beam = BeamSpotEllipse(10000,xr=10,pxr=1)
    PlotxPxHist(Beam,20)
    print "graph 3 is plotted now"
    plt.pause(5)  # wait 5s and view graph

if 1==1:
    Beam = BeamSpotBlock(1000,xc=-2,xr=4,pxc=9,pxr=1)
    PlotxPx(Beam)
    plt.draw()
    print "graph 4 is plotted now"

print "end script now"

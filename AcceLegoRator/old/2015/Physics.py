import numpy as np
import math

class Constants():
	def __init__(self):

		self.pmass = 938.272013e6 # eV/c**2
		self.clight = 299792458 # m/s
		self.echarge = 1.60217653e-19 # C
		self.emass = 0.51099892e6 # eV/c**2
		self.pomass = self.emass
		self.Brho1GeV = 1e9 / self.clight # T.m / GeV/

	def PMass(self):
            return self.pmass
	
	def EMass(self):
            return self.emass

	def CLight(self):
            return self.clight

	def ECharge(self):
            return self.echarge


class Physics():
  	def __init__(self,*args):
  
                self.pname = args[0]	
                self.ene = args[1]
		self.GetPartMass()

	def GetPartMass(self):
                if (self.pname=='proton'):
                        ParticleMass = Constants().PMass()
                else: 
                        ParticleMass = Constants().EMass()    
			
                return ParticleMass

	    
	def GetPartMom(self):
            mass = self.GetPartMass()
            GammaRel = (self.ene + mass)/mass
	    BetaRel = math.sqrt(1-(1/pow(GammaRel,2)));
	    Momentum = GammaRel * BetaRel * mass	
            
	    return Momentum
	    
	    
# Local Variables:
# mode: Python
# python-indent: 8
# tab-width: 8
# indent-tabs-mode: nil
# End:

import numpy as np
import matplotlib.pylab as plt
from matplotlib.patches import Ellipse
import math
import random

class Beam():
    def __init__(self, *args):
	self.particles=[]
        self.AddParticle(0,0,0,0,0,0)
	self.BeamGauss(*args[1:8])
 	self.partMass = args[8]
        self.partMom = args[9]      

    def AddParticle(self,x,px,y,py,z,pz):
        """add one particle with given parameter to beam"""
        self.particles.append(np.array([x,px,y,py,z,pz],dtype = np.float_))
      
    def BeamGauss(self, n, xr=0 , pxr=0, yr=0 , pyr=0, zr=0, pzr=0,
                  	xc=0 , pxc=0, yc=0 , pyc=0, zc=0, pzc=0):
        """Create a Beam (list of n Particles) with Gaussian distribution 
             width and center can be specified in all dimension
             and each is independend from all other"""
        for i in range(n):
            self.AddParticle(
              xc  + (np.random.normal(0,xr)  if xr  != 0 else 0),
              pxc + (np.random.normal(0,pxr) if pxr != 0 else 0),
              yc  + (np.random.normal(0,yr)  if yr  != 0 else 0),
              pyc + (np.random.normal(0,pyr) if pyr != 0 else 0),
              zc  + (np.random.normal(0,zr)  if zr  != 0 else 0),
              pzc + (np.random.normal(0,pzr) if pzr != 0 else 0) ) 

    def PlotEllipse(self, num=None):
        emitx = 40
        Beta = 0.1125
        Alpha = -1
        Gamma = (1+pow(Alpha,2))/Beta
        Phi = 0.5*math.atan(2*Alpha/(Gamma-Beta))*180/3.14
        plt.figure()
        ax = plt.gca()
	ellipse = Ellipse(xy=(0,0), width=math.sqrt(Beta*emitx), height=math.sqrt(emitx*Gamma), angle = Phi,
                           edgecolor='r', fc='None', lw=2,)
        ax.add_patch(ellipse)
        plt.show()

    def PlotEneHisto(self, num=None):
	eneVec=[]
	for parti in self.particles:
	    P = (1+parti[5])*self.partMom
            eneVec.append((math.sqrt(pow(P,2)+pow(self.partMass,2))-self.partMass)/1E6)
	fig=plt.figure(num=num)
	plt.ylabel('counts')
        plt.xlabel('Energy (MeV)')
	plt.hist(eneVec)
    
    def Plot1DPhaseSpace(self,dimension,style=None, LabelX=None, LabelY =None):
        xdim=dimension*2

	if (LabelY == 'y'):
	   ydim=2
 	   UnitsY = ' [m]'
	else:
           ydim=dimension*2+1
	   UnitsY = ' [BetaGamma]'

        myx=[]
        mypx=[]  
        for parti in self.particles:
            myx.append(parti[xdim])
            mypx.append(parti[ydim])
        if (style is None):
            style='go'
	plt.xlabel(LabelX + ' [m]')
	plt.ylabel(LabelY + UnitsY)
        plt.plot(myx,mypx,style)	

    def PlotXPx(self,style=None, LabelX=None, LabelY=None):   
        self.Plot1DPhaseSpace(0, style=style, LabelX=LabelX, LabelY=LabelY)

    def PlotYPy(self,style=None, LabelX=None, LabelY=None):   
        self.Plot1DPhaseSpace(1, style=style, LabelX=LabelX, LabelY=LabelY)
  
    def PlotZPz(self,style=None, LabelX=None, LabelY=None):   
        self.Plot1DPhaseSpace(2, style=style, LabelX=LabelX, LabelY=LabelY) 

    def PlotXY(self,style=None, LabelX=None, LabelY=None):   
        self.Plot1DPhaseSpace(0, style=style, LabelX=LabelX, LabelY=LabelY) 

    def PlotTransPlane(self,num=None):
        fig=plt.figure(num=num)
    	fig.suptitle("Particles distribution in the transversal plane", fontsize=16)
        ax = plt.subplot(1,3,1)
	ax.set_title('Horizontal plane')
        self.PlotXPx(style='ro', LabelX='x', LabelY='px')
	ax = plt.subplot(1,3,2)
	ax.set_title('Vertical plane')
        self.PlotYPy(style='bo', LabelX='y', LabelY='py')
 	ax = plt.subplot(1,3,3)
	ax.set_title('Beam Profile')
        self.PlotXY(style='go', LabelX='x', LabelY='y')
	plt.show()

    def PlotLongPlane(self,num=None):
        fig=plt.figure(num=num)
	fig.suptitle("Particles distribution in the longitudinal plane", fontsize=16)
        ax = plt.subplot(1,2,1)
	ax.set_title('Longitudinal plane')
        self.PlotZPz(style='ro', LabelX='z', LabelY='pz')
	ax = plt.subplot(1,2,2)
	ax.set_title('Energy distribution')
        self.PlotEneHisto(0)
	plt.show()


    
        
    
	

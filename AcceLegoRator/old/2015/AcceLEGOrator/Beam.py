import numpy as np
import matplotlib.pylab as plt
import math
import random

class Beam():
    def __init__(self, *args):
	self.particles=[]
        self.AddParticle(0,0,0,0,0,0)
	self.BeamGauss(*args[1:7])
        self.partMass = args[8]

    def AddParticle(self,x,px,y,py,z,pz):
        """add one particle with given parameter to beam"""
        self.particles.append(np.array([x,px,y,py,z,pz],dtype = np.float_))
      
    def BeamGauss(self, n, xr=0 , pxr=0, yr=0 , pyr=0, zr=0, pzr=0,
                  	xc=0 , pxc=0, yc=0 , pyc=0, zc=0, pzc=0):
        """Create a Beam (list of n Particles) with Gaussian distribution 
             width and center can be specified in all dimension
             and each is independend from all other"""
        
        # you can add other particle distribution...
        
        #some plots...
    
    def PlotEneHisto(self, num=None):
        #display the energy distribution of the beam
        #I'm still working on this point

        #some plots....

    def Plot1DPhaseSpace(self,dimension,style=None, LabelX=None, LabelY =None):
        #display one phace space at time

    def PlotTransPlane(self,num=None):
        #display transversal Phase Space (x,px), (y,py) and the beam profile (x,y)

    def PlotLongPlane(self,num=None):
        #display longitudinal Phase Space (z,pz) and energy distribution
        
    
        
    
	

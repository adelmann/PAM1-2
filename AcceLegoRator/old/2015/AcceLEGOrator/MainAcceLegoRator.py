"""
@authors: Dr. Jochen Krempel
	  Valeria Rizzoglio
	  Dr. Andreas Adelmann
"""

import numpy as np
import matplotlib.pylab as plt
import math
import random

from Beam import Beam
from Physics import Physics
from Physics import Constants

# ------------------------
# 1- Select the particle
# ------------------------

# write code for: 
# 1- select  particle_type and kinetic energy in eV from Physics class

# 2- return particle mass in eV/c**2
# 3- return particle momentum in eV/c

# ------------------------------------
# 2- Define Gaussian-distributed beam
# ------------------------------------

# add code for defining the beam distribution from Beam class
# n.primaries, sigmax [m], sigmapx [betagamma]..., 

# ------------------------
# 3- Plot the phase space
# ------------------------

#plot transversal plane (x, px), (y, py) and beam profile (x,Y)



"""
@authors: Dr. Jochen Krempel
	  Valeria Rizzoglio
	  Dr. Andreas Adelmann
"""

import numpy as np
import matplotlib.pylab as plt
import math
import random

class Physics():
	def __init__(self,*args):
  
                self.pmass = args[0]	
                self.ene = args[1]

	def GetPartMom(self):
            GammaRel = (self.ene + self.pmass)/self.pmass
	    BetaRel = math.sqrt(1-(1/pow(GammaRel,2)));
	    Momentum = GammaRel * BetaRel * self.pmass	
            
	    return Momentum

energy = 230.0
mass = 938.272013

PartInfo = Physics(mass,energy)
PartMom = PartInfo.GetPartMom() 
print PartMom



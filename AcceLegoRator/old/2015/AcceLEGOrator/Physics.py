import numpy as np
import math

class Constants():
	def __init__(self):

		self.pmass = 938.272013e6 # eV/c**2
		self.clight = 299792458 # m/s
		self.echarge = 1.60217653e-19 # C
		self.emass = 0.51099892e6 # eV/c**2
		self.pomass = self.emass
		self.Brho1GeV = 1e9 / self.clight # T.m / GeV/

	def PMass(self):
            return self.pmass
	
	def EMass(self):
            return self.emass

	def CLight(self):
            return self.clight

	def ECharge(self):
            return self.echarge


class Physics():
  	def __init__(self,*args):
  
		self.pname = args[0]	#first argument contaning the type of particle
		self.ene = args[1]      #second argument containing the kinetic energy of the beam
		self.GetPartMass()
		self.GetPartMom()

	def GetPartMass(self):
		ParticleMass = 0.0
		# according to the selected type of particle in the MainAcceLegoRator 
		#return the corresponding mass from Constants() class	
		return ParticleMass

	    
	def GetPartMom(self):
		Momentum = 0.0
		# according to the ParticleMass and kinetic energy selected
		# return the reference momentum of the beam
		return Momentum
	    

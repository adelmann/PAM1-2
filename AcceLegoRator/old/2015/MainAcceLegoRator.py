"""
@authors: Dr. Jochen Krempel
	  Valeria Rizzoglio
	  Dr. Andreas Adelmanm
"""

import numpy as np
import matplotlib.pylab as plt
import math
import random

from Beam import Beam
from Physics import Physics
from Physics import Constants

# ------------------------
# 1- Select the particle
# ------------------------

# particle_type and kinetic energy in eV
PartInfo = Physics("proton", 230e6)

PartMass = PartInfo.GetPartMass()  # return reference particle mass in eV/c**2
PartMom = PartInfo.GetPartMom()    # return reference particle momentum in eV/c

# ------------------------------------
# 2- Define Gaussian-distributed beam
# ------------------------------------

# n.primaries, sigmax [m], sigmapx [], 
InputBeam = Beam("gauss",4000,1.2,0.3,2.1,0.5,0.5,0.02,PartMass,PartMom)

# ------------------------
# 3- Plot the phase space
# ------------------------
InputBeam.PlotTransPlane(0)
InputBeam.PlotLongPlane(0)
InputBeam.PlotEllipse(0)



# -*- coding: utf-8 -*-
"""

@author: krempel
"""


import warnings
import matplotlib.cbook
warnings.filterwarnings("ignore",category=matplotlib.cbook.mplDeprecation)
# The 3 lines above are to get rid of a warning that appears on the default 
# windows installation when we use plt.pause()
#   MatplotlibDeprecationWarning: Using default event loop until function specific to this GUI is implemented
#    warnings.warn(str, mplDeprecation)

#import numpy as np  # NumPy (multidimensional arrays, linear algebra, ...)
#import scipy as sp  # SciPy (signal and image processing library)
#import matplotlib as mpl         # Matplotlib (2D/3D plotting library)
#import matplotlib.pyplot as plt  # Matplotlib's pyplot: MATLAB-like syntax
from pylab import *              # Matplotlib's pylab interface
#ion()                            # Turned on Matplotlib's interactive mode
#import guidata  # GUI generation for easy dataset editing and display



electron_mass = 0.51099892e6 # eV/c**2
positron_mass = electron_mass
proton_mass   = 938.272013e6 # eV/c**2
c_light = 299792458 # m/s
e_charge = 1.60217653e-19 # C
Brho1GeV = 1e9 / c_light # T.m / GeV/c

def SetUnit(system):
    global c_light
    if system.upper()=='NATURAL':
        c_light = 1
    elif  system.upper()=='SI':
        c_light = 299792458 # m/s
    else:
        raise NamedError("unknown Unit system'" + system + "'", 
             "only 'SI' and 'natural' are implemented")

def sol():   # SpeedOfLight
    return c_light             

import numpy as np
import matplotlib.pylab as plt
import math
import random
import copy
from itertools import repeat

if ((not 'debugging' in locals()) and (not 'debugging' in globals())):
    debugging=0

#Warning: the following does not work when invoking with "from task_5_sol import *"
global GlobalRefMass
if ((not 'GlobalRefMass' in locals()) and (not 'GlobalRefMass' in globals())):
    GlobalRefMass=None
global GlobalRefGamma
if ((not 'GlobalRefGamma' in locals()) and (not 'GlobalRefGamma' in globals())):
    GlobalRefGamma=None
global GlobalRefMomentum
if ((not 'GlobalRefMomentum' in locals()) and (not 'GlobalRefMomentum' in globals())) :
    GlobalRefMomentum=None
global GlobalRefEnergy
if ((not 'GlobalRefEnergy' in locals()) and (not 'GlobalRefEnergy' in globals())):
    GlobalRefEnergy=None
global GlobalRefBeta
if ((not 'GlobalRefBeta' in locals()) and (not 'GlobalRefBeta' in globals())):
    GlobalRefBeta=None
global GlobalRefCharge
if ((not 'GlobalRefCharge' in locals()) and (not 'GlobalRefCharge' in globals())):
    GlobalRefCharge=None

class Beam():
    def __init__(self, *args):
        self.particles=[]
        self.AddParticle(0,0,0,0,0,0)
        if len(args) >0:
            if (args[0]=='normal'):
                self.BeamNormal(*args[1:])
            if (args[0]=='ellipse'):
                self.BeamPairwiseHollowEllipse(*args[1:])   
        #todo:
        # ref energy
        # ref gamma
        # ref beta
        
    def AddParticle(self,x,px,y,py,z,pz):
        """add one particle with given parameter to beam"""
        self.particles.append(np.array([x,px,y,py,z,pz],dtype = np.float_))
        #return self  # not really necessary, as Python is call-by-object
    
    def RemoveAllParticles(self):
        self.__init__()
 
    def BeamPairwiseEllipseLine(self, n, 
                  xr=0, pxr=0, xphi=0,
                  yr=0, pyr=0, yphi=0,
                  zr=0, pzr=0, zphi=0,
                  xc=0 , pxc=0, yc=0 , pyc=0, zc=0, pzc=0
                              ):                                
        """Create a Beam (list of n Particles) in pairwise ellipse shape (thin line)
            we use a parametric description thus very efficient
            x(t) =xr *cos(t); px(t)=pxr*cos(t+xphi)  and correspndingly for other dimensions
            xphi determines the orientation of the ellipse and must be given in radian"""

        for i in range(n):
            t=random.random()*2*np.pi
            x=xr*np.cos(t)
            px=pxr*np.sin(t+xphi)
            t=random.random()*2*np.pi
            y=yr*np.cos(t)
            py=pyr*np.sin(t+yphi)
            t=random.random()*2*np.pi
            z=zr*np.cos(t)
            pz=pzr*np.sin(t+zphi)
            self.AddParticle(
                      xc+x    ,       pxc+px  ,
                      yc+y    ,       pyc+py  ,
                      zc+z    ,       pzc+pz   )
            
    def BeamPairwiseHollowEllipse(self, n, 
                  xr=0 , pxr=0, yr=0 , pyr=0, zr=0, pzr=0,
                  dx=0.1, dy=0.1, dz=0.1,
                  xc=0 , pxc=0, yc=0 , pyc=0, zc=0, pzc=0
                              ):
        """Create a Beam (list of n Particles) in pairwise ellipsoid shape
            we use poor man random number selection that makes it extremely slow
            furthermore it looks thinner on major axis, ==> most likely buggy"""
        def RandomEllipse(a,b,d):
            radius= max( a,b)
            Sum=-2*d
            while Sum > (1+d/2)**2 or Sum < (1-d/2)**2 :   #Dice until point is inside ellipse
                Sum=0
                x1 = (random.random()*2-1) * radius
                x2 = (random.random()*2-1) * radius 
                Sum += x1*x1 / (a*a)
                Sum += x2*x2 / (b*b)
            return (x1,x2)
        for i in range(n):
            x,px = RandomEllipse(xr,pxr,dx)
            y,py = RandomEllipse(yr,pyr,dy)
            z,pz = RandomEllipse(zr,pzr,dz)
            self.AddParticle(
                      xc-x    ,       pxc-px  ,
                      yc-y    ,       pyc-py  ,
                      zc-z    ,       pzc-pz   )
                
    def BeamNormal(self, n, xr=0 , pxr=0, yr=0 , pyr=0, zr=0, pzr=0,
                  xc=0 , pxc=0, yc=0 , pyc=0, zc=0, pzc=0):
        """Create a Beam (list of n Particles) with Gaussian distribution 
             width and center can be specified in all dimension
             and each is independend from all other"""
        for i in range(n):
            self.AddParticle(
              xc  + (np.random.normal(0,xr)  if xr  != 0 else 0),
              pxc + (np.random.normal(0,pxr) if pxr != 0 else 0),
              yc  + (np.random.normal(0,yr)  if yr  != 0 else 0),
              pyc + (np.random.normal(0,pyr) if pyr != 0 else 0),
              zc  + (np.random.normal(0,zr)  if zr  != 0 else 0),
              pzc + (np.random.normal(0,pzr) if pzr != 0 else 0)  ) 
        #return Beam    
        
    def PlotTriple(self,num=None):
        plt.figure(num=num)
        plt.subplot(2,2,1)
        self.PlotXPx(style='ro')
        plt.subplot(2,2,2)
        self.PlotYPy(style='go')
        plt.subplot(2,2,3)
        self.PlotZPz(style='bo')
        
    def PlotXPx(self,style=None):   
        self.Plot1DPhaseSpace(0, style=style)
    def PlotYPy(self, **kwargs):   # same as "style=None" above, but be flexible for future extensions
        self.Plot1DPhaseSpace(1, **kwargs)
    def PlotZPz(self, **kwargs):   
        self.Plot1DPhaseSpace(2, **kwargs)
        
    def Plot1DPhaseSpace(self,dimension,style=None):
        xdim=dimension*2
        ydim=dimension*2+1
        myx=[]
        mypx=[]  
        for parti in self.particles:
            myx.append(parti[xdim])
            mypx.append(parti[ydim])
        if (style is None):
            style='go'
        plt.plot(myx,mypx,style)

    def CalcEmittance(self, debug=debugging):
        center = np.array([0,0,0,0,0,0],dtype = np.float_)
        for p in self.particles:
            center += p
        center/=len(self.particles)
        Mcore = np.matrix([[0,0,0,0,0,0],
                           [0,0,0,0,0,0],
                           [0,0,0,0,0,0],
                           [0,0,0,0,0,0],
                           [0,0,0,0,0,0],
                           [0,0,0,0,0,0]],dtype = np.float_)
        for p in self.particles:
            Mcore += np.dot(np.matrix(p-center).transpose(),  (np.matrix(p-center)))
        Mcore/=len(self.particles)
        if debug != 0:
            print Mcore
        sigma = math.pi*math.sqrt(np.linalg.det(Mcore))
        return sigma


            
class LineElement(object):
    """The Element base class.  All beamline element classes should
    inherit from this class"""
    Length=0
    Map=None
    q=None
    RefMass=None
    RefMomentum=None
    RefGamma=None
    RefEnergy=None
    RefBeta=None
    SetRefMass=None
    SetRefMomentum=None
    SetRefGamma=None
    SetRefEnergy=None
    SetRefBeta=None
    setRefValuesChanged=True
    ElementNumber=-1     #This will be set by the append() function of the  Line class

    def __init__(self, name=None, L=1, S=-1, Q=None):
        self.name = name                                         # Element name
        self.Length = L                                          # Element length
        self.S = S                                               # S position of start of element
        if not Q is None:
            self.q=Q
        self.offset = np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0])   # (x,px,y,py,z,P)
        self.x = np.zeros(6)
     #   self.twiss = Twiss()
        # if self.Map is None:
            # self.Map=np.array([
                # [1, 0, 0, 0, 0, 0],
                # [0, 1, 0, 0, 0, 0],
                # [0, 0, 1, 0, 0, 0],
                # [0, 0, 0, 1, 0, 0],
                # [0, 0, 0, 0, 1, 0],
                # [0, 0, 0, 0, 0, 1],
                # ])
        #self.updateMap()
    def getMap(self):
        if self.Map is None:
            self.updateMap()
        return self.Map
    
    def setRefValues(self, Gamma=None, Momentum=None, Energy=None, Beta=None, Mass=None, Charge=None):
        if  (not Momentum is None) and (Gamma >0):         
            self.SetRefGamma=Gamma
        if  (not Momentum is None) and (Momentum >0):         
            self.SetRefMomentum=Momentum
        if  (not Energy is None) and (Energy >0):         
            self.SetRefEnergy=Energy
        if  (not Beta is None) and (Beta >0):         
            self.SetRefBeta=Beta
        if  (not Mass is None) and (Mass >0):         
            self.SetRefMass=Mass
        if  (not Charge is None):         # Charge may be negative (not recommended)
            self.q=Charge
        self.setRefValuesChanged=True    
            
    def updateRefValues(self):
        if self.setRefValuesChanged:
            global GlobalRefGamma, GlobalRefMomentum, GlobalRefEnergy, GlobalRefBeta
            source=None
            if (not self.SetRefMass is None) and (self.SetRefMass >0):
                 self.RefMass=self.SetRefMass 
            else:
                if ('GlobalRefMass' in globals()) and ( not GlobalRefMass is None) and (GlobalRefMass > 0) :
                    self.RefMass = GlobalRefMass        
                else:
                    self.RefMass = proton_mass        
                    print "WARNING in Magnet: use proton_mass as default mass"
            #Determine source from which to obtain design velocity/momentum/enrgy/gamma        
            #First check class variables
            if  (not self.SetRefGamma is None) and (self.SetRefGamma >0):         #DesignGamma factor
                self.RefGamma=self.SetRefGamma
                source='RefGamma'
            elif  (not self.SetRefMomentum is None) and (self.SetRefMomentum >0):         #DesignGamma factor
                self.RefMomentum=self.SetRefMomentum
                source='RefMomentum'
            elif  (not self.SetRefEnergy is None) and (self.SetRefEnergy >0):         #DesignGamma factor
                self.RefEnergy=self.SetRefEnergy
                source='RefEnergy'
            elif  (not self.SetRefBeta is None) and (self.SetRefBeta >0):         #DesignGamma factor
                self.RefBeta=self.SetRefBeta
                source='RefBeta'
            #Nothing found, check now for Global Variables
            elif  (not GlobalRefGamma is None) and (GlobalRefGamma >0):         #DesignGamma factor
                self.RefGamma=GlobalRefGamma
                source='RefGamma'
            elif (not GlobalRefMomentum is None) and (GlobalRefMomentum >0):    # Design momentum 
                self.RefMomentum = GlobalRefMomentum
                source='RefMomentum'
            elif (not GlobalRefEnergy is None) and (GlobalRefEnergy >0):      # DesignEnergy
                self.RefEnergy=GlobalRefEnergy
                source='RefEnergy'
            elif (not GlobalRefBeta is None) and (GlobalRefBeta >0):              #design velocity
                self.RefBeta=GlobalRefBeta
                source='RefBeta'
            
            #Calculate missing values from the one source (+ mass)
            if  source=='RefGamma':         #DesignGamma factor
                self.RefBeta=math.sqrt(1-1/self.RefGamma**2)
                self.RefMomentum = self.RefGamma* c_light  * self.RefBeta * self.RefMass     
                self.RefEnergy=self.RefMass * self.RefGamma * c_light**2
            elif source=='RefMomentum':    # Design momentum 
                self.RefGamma=math.sqrt((self.RefMomentum/self.RefMass/c_light)**2+1)
                self.RefBeta=math.sqrt(1-1/self.RefGamma**2)
                self.RefEnergy= math.sqrt((self.RefMomentum * c_light) **2 +  (self.RefMass *  c_light**2)**2 )
            elif  source=='RefEnergy':      # DesignEnergy
                self.RefGamma=self.RefEnergy / self.RefMass / c_light**2
                self.RefBeta=math.sqrt(1-1/self.RefGamma**2)
                self.RefMomentum = math.sqrt((self.RefEnergy * c_light) **2 -  (self.RefMass *  c_light**2)**2 )
            elif source=='RefBeta':              #design velocity
                self.RefGamma=1/math.sqrt(1-self.RefBeta**2)
                self.RefMomentum = self.RefGamma* c_light  * self.RefBeta * self.RefMass         
                self.RefEnergy=self.RefMass * self.RefGamma * c_light**2
            else:
                raise NameError("No design Energy,Momentum, or velocity specified in '" + 
                                self.__class__.__name__ +  "' on Position " + str(self.ElementNumber))
            if self.q is None:
                if not GlobalRefCharge is None:
                    self.q=GlobalRefCharge
                else:
                    self.q=e_charge
                    print "WARNING: in Magnet, assume charge = +1e"
            print "RecalculatingReferenceValues of '" + self.__class__.__name__ + \
                    "' on Position " + str(self.ElementNumber)
            self.setRefValuesChanged=False

            
    def updateMap(self):
        raise NameError("Each element must provide its own update function in '"+ 
                       self.__class__.__name__ +  "' on Position " + str(self.ElementNumber))
        
    def TrackThroughElement(self,Beam):
        """track a full beam through a single element
        particle list of beam will be overwritten"""
        if self.Map is None:
            self.updateMap()
        newparticles=[]
        for item in Beam.particles:
            item = np.dot(self.getMap(),item)
            newparticles.append(item)
        Beam.particles=newparticles        


class DriftSpace(LineElement):
    def __init__(self, L=0, **kwargs):
        #self.Length=Length
        LineElement.__init__(self, L=L, **kwargs)

    def updateMap(self):
        self.updateRefValues()
        self.Map=np.array([
            [1, self.Length, 0, 0, 0, 0],
            [0, 1, 0, 0, 0, 0],
            [0, 0, 1, self.Length, 0, 0],
            [0, 0, 0, 1, 0, 0],
            [0, 0, 0, 0, 1, self.Length/((self.RefBeta*self.RefGamma)**2)],
            [0, 0, 0, 0, 0, 1],
            ])


class BasicMagnet(LineElement):
    """A class describing the operation of magnets.  Inherits from Element."""
    def __init__(self,  B=0, tilt=0, **kwargs):
        self.B = B
        self.tilt = tilt
        LineElement.__init__(self, **kwargs)

    def SetTilt(self, tilt) :
        """Sets the tilt of the element"""
        self.tilt = tilt

    def GetTilt(self) :
        """Returns the tilt of the element"""
        return self.tilt
    
    #def TrackThruEle(self, beam_in):
     #   pass
 
class Dipole(BasicMagnet):
    #h = 1.6021773e-10  #h = 1 + kx + ky... curvature obligatorily given in GeV^-1. 1/1m = 1.6021773e-10GeV^-1.
    #h=0.1
    #todo: probably better to have omega outside, and various possibility to define it.
    #  needs then its own update function
    #def __init__(self, name=None, L=1, P=1, S=0, B=0, tilt=0):
    def __init__(self, B=None, r0=None, h=None, k=None, **kwargs):
        self.focusPriority =[]
        if (not B is None):
            self.B = B
            self.focusPriority.append("B")
        if (not r0 is None):
            self.r0 = r0
            self.focusPriority.append("r0")
        if (not h is None):
            self.h = h
            self.focusPriority.append("h")
        if (not k is None):
            self.k = k
            self.focusPriority.append("k")
        BasicMagnet.__init__(self, B=B, **kwargs)
    def SetB(self, B) :
        """Sets the B field of the element"""
        self.B = B
        self.focusPriority.append("B")
    def GetB(self) :
        """Returns the B field of the element"""
        return self.B
    def update(self):
        self.CalcOmega()
        omega = self.omega
        COS = math.cos(omega*self.Length)
        SIN = math.sin(omega*self.Length)
        self.Map = np.array([
                    [COS,             SIN/omega,    0,          0,          0,         (1-COS)/(omega*self.RefBeta)],
                    [-omega*SIN,      COS,          0,          0,          0,         SIN/self.RefBeta],
                    [0,             0,          1,          self.Length,0,         0],
                    [0,             0,          0,          1,          0,         0],
                    [-SIN/self.RefBeta, -(1-COS)/(omega*self.RefBeta),0,0,      1,         1/((self.RefBeta*self.RefGamma)**2)-(omega*self.Length-SIN)/(omega*self.RefBeta*self.RefBeta)],
                    [0,             0,          0,          0,          0,         1]
                    ])
    updateMap=update
    def CalcOmega(self):
        self.updateRefValues()
        if "B" in self.focusPriority:
            self.k= self.q / self.RefMomentum * self.B
        if "r0" in self.focusPriority:   
            self.h= 1 / self.r0
        if not ("B" in self.focusPriority or "k" in self.focusPriority):
            self.omega = self.h     #We assume a matched dipole
        elif not ("h" in self.focusPriority or "r0" in self.focusPriority):
            self.omega = self.k     #We assume a matched dipole
        elif ("B" in self.focusPriority or "k" in self.focusPriority) and ("h" in self.focusPriority or "r0" in self.focusPriority):
            self.omega = np.sqrt(abs( self.h * self.k))  
        else:
            raise NameError("Dipole, no information how to calculate curvature '" + 
                    self.__class__.__name__ +  "' on Position " + str(self.ElementNumber))
        self.signB = np.sign(self.k)    



class Quadrupole(BasicMagnet):
    """Quad class, do not use directly, for inheritance only"""
    def CalcRmat(self, P=-1):
        """outdated,  not used any more:
        Calculate the R matrix of the element
        We assume an upright (non-skew) magnet.
        The skew quadrupole class will overwrite this function"""
        if P == -1:
            P = self.RefMomentum
        L = self.Length
        if self.B == 0:
             self.Map=DriftSpace(self.Length).getMap()
             #self.R = DriftRmat(L)
        else:
        #following script D.4    
            signB=np.sign(self.B);
            Brho = Brho1GeV * P
            omega = np.sqrt(SignB * (self.B / self.Length ) / Brho)
        #k1=q / P0 *b2 / r0
        #omega = np.sqrt(k1)
        COS = math.cos(omega*self.Length)
        SIN = math.sin(omega*self.Length)
        COSHYP = math.cosh(omega*self.Length)
        SINHYP = math.sinh(omega*self.Length)
        self.Map = np.array([
                    [COS,              SIN/omega, 0,              0,            0,  0],
                    [-signB*omega*SIN, COS,       0,              0,            0,  0],
                    [0,                0,         COSHYP,         SINHYP/omega, 0,  0],
                    [0,                0,         omega * SINHYP, COSHYP,       0,  0],
                    [0,                0,         0,              0,            1,  self.Length/((self.RefBeta*self.RefGamma)**2)],
                    [0,                0,         0,              0,            0,  1]
                    ])        
    #updateMap=CalcRmat
    
class QuadrupoleUpright(Quadrupole):
    """Upright (non-skew) Quadrupoles, 
    This should not be called directly, but the inheriting classes"""
    def updateMap(self):
        """Calculate the R matrix of the element
        for an upright (non-skew) magnet."""
        self.updateRefValues()
        self.CalcOmega()
        omega=self.omega
        signB=self.signB
        #following script D.4    
        #    signB=np.sign(self.B);
        #    Brho = Brho1GeV * P
        #    omega = np.sqrt(SignB * (self.B / self.Length ) / Brho)
        #k1=q / P0 *b2 / r0
        #omega = np.sqrt(k1)
        COS = math.cos(omega*self.Length)
        SIN = math.sin(omega*self.Length)
        COSHYP = math.cosh(omega*self.Length)
        SINHYP = math.sinh(omega*self.Length)
        if signB > 0:
            self.Map = np.array([
                    [COS,          SIN/omega, 0,              0,            0,  0],
                    [-omega*SIN,   COS,       0,              0,            0,  0],
                    [0,            0,         COSHYP,         SINHYP/omega, 0,  0],
                    [0,            0,         omega * SINHYP, COSHYP,       0,  0],
                    [0,            0,         0,              0,            1,  self.Length/((self.RefBeta*self.RefGamma)**2)],
                    [0,            0,         0,              0,            0,  1]
                    ])   
        else:
            self.Map = np.array([
                    [COSHYP,         SINHYP/omega, 0,           0,          0,  0],
                    [omega * SINHYP, COSHYP,       0,           0,          0,  0],
                    [0,                0,          COS,         SIN/omega,  0,  0],
                    [0,                0,         -omega*SIN,   COS,        0,  0],
                    [0,                0,         0,            0,          1,  self.Length/((self.RefBeta*self.RefGamma)**2)],
                    [0,                0,         0,            0,          0,  1]
                    ])   
            
    def CalcOmega(self):
        raise NameError("Error in QuadrupoleUpright.CalcOmega(). " +
                "Don't know how to calc omega. You must use sub-class! '"+ 
                self.__class__.__name__ +  "' on Position " + str(self.ElementNumber))

class QuadrupoleSkew(Quadrupole):
    """Skew Quadrupoles, 
    This should not be called directly, but the inheriting classes"""
    def updateMap(self):
        """Calculate the R matrix of the element
        for a skew magnet."""
        self.updateRefValues()
        self.CalcOmega()
        omega=self.omega
        signB=self.signB
        #following script D.4    
        #    signB=np.sign(self.B);
        #    Brho = Brho1GeV * RefMomentum
        #    omega = np.sqrt(SignB * (self.B / self.Length ) / Brho)
        #k1=q / P0 *b2 / r0
        #omega = np.sqrt(k1)
        COS = math.cos(omega*self.Length)
        SIN = math.sin(omega*self.Length)
        COSHYP = math.cosh(omega*self.Length)
        SINHYP = math.sinh(omega*self.Length)
        if signB > 0:
            self.Map = 0.5 * np.array([
                    [COS+COSHYP,               (SIN +SINHYP)/omega,  COS - COSHYP,         (SIN -SINHYP)/omega,   0, 0 ],
                    [-(SIN-SINHYP)*omega,      COS+COSHYP,           (SIN +SINHYP)*omega,   COS - COSHYP,         0, 0],
                    [COS - COSHYP,             (SIN -SINHYP)/omega,  COS + COSHYP,         (SIN +SINHYP)/omega,   0, 0 ],
                    [-(SIN + SINHYP)*omega,    COS - COSHYP,         (SIN -SINHYP)*omega,   COS + COSHYP,         0, 0 ],
                    [0,                        0,                    0,                    0,                     2,  2* self.Length/((self.RefBeta*self.RefGamma)**2)],
                    [0,                        0,                    0,                    0,                     0,  2]
                    ])   
        else:
            raise NameError("Error: Skew Quadrupole with negative moment not implemented '" + 
                         self.__class__.__name__ +  "' on Position " + str(self.ElementNumber))
    def CalcOmega(self):
        raise NameError("Error in QuadrupoleUpright.CalcOmega(). " +
                    "Don't know how to calc omega. You must use sub-class! '" + 
                    self.__class__.__name__ +  "' on Position " + str(self.ElementNumber))
        

class QuadrupoleUprightMoments(QuadrupoleUpright):
    """Upright (non-skew) Quadrupoles, 
    in case moments of B-field are provided to define the magnet"""
    def __init__ (self, L, b2, r0, **kwargs):
        self.b2=b2
        self.r0=r0
        self.Length=L
        BasicMagnet.__init__(self, L=L, **kwargs)
    def CalcOmega(self):
        k1=self.q / self.RefMomentum *self.b2 / self.r0 
        self.omega = np.sqrt(abs(k1))
        self.signB = np.sign(k1)            
        
class QuadrupoleUprightGrad(QuadrupoleUpright):
    def __init__ (self, L, k, **kwargs):
        self.k=k
        self.Length=L
        BasicMagnet.__init__(self, L=L, **kwargs)
    def CalcOmega(self):
        self.omega = np.sqrt(abs(self.k))   
        self.signB = np.sign(self.k)        
        
class QuadrupoleSkewMoments(QuadrupoleSkew):
    """Upright (non-skew) Quadrupoles, 
    in case moments of B-field are provided to define the magnet"""
    def __init__ (self, L, a2, r0, **kwargs):
        self.a2=a2
        self.r0=r0
        self.Length=L
        BasicMagnet.__init__(self, L=L, **kwargs)
    def CalcOmega(self):
        k1=self.q / self.RefMomentum *self.a2 / self.r0 
        self.omega = np.sqrt(abs(k1))
        self.signB = np.sign(k1)            
        
class QuadrupoleSkewGrad(QuadrupoleSkew):
    def __init__ (self, L, k, **kwargs):
        self.k=k
        self.Length=L
        BasicMagnet.__init__(self, L=L, **kwargs)
    def CalcOmega(self):
        self.omega = np.sqrt(abs(self.k))   
        self.signB = np.sign(self.k)        
       
    
    
class QuadrupoleThin(BasicMagnet):
    """Quad class"""
    def __init__ (self, L=0, f=None, **kwargs):
        self.f=f
        self.Length=L
        BasicMagnet.__init__(self, L=L, **kwargs)    
    def updateMap(self):
        """Calculate the R matrix of the element"""
        #self.updateRefValues()
        if self.f is None or self.f==0:
            raise  NameError("Thin Quadrupole without focal length '"+ 
                        self.__class__.__name__ +  "' on Position " + str(self.ElementNumber))
        self.Map = np.array([
                [1,         0,    0,         0,  0,  0],
                [-1/self.f, 1,    0,         0,  0,  0],
                [0,         0,    1,         0,  0,  0],
                [0,         0,    1/self.f,  1,  0,  0],
                [0,         0,    0,         0,  1,  0],
                [0,         0,    0,         0,  0,  1]
                ])        

    
class Line(LineElement,list):
    RefGamma=None
    RefMomentum=None
    RefEnergy=None
    RefBeta=None
    RefMass=None
    RefCharge=None
    setRefValuesChanged=False
        
    def __mul__(self, fact):
        """Allows multiplication of a small lattice subset by an integer in 
        order to easily define a repeated section"""
        new_line = Line()
        copyfunc = lambda x: new_line.extend(copy.deepcopy(x))
        for rep in repeat(copyfunc, fact):
            rep(self)
        return new_line

    def __repr__(self):
        def namecatch(inst):
            try: return str(inst.name)
            except AttributeError: return "No name attr"

        ret = '\n'.join(namecatch(ele)+" :: "+str(ele.__class__) for ele in self)
        return ret

    def append(self,value):
        list.append(self,value)
        value.ElementNumber=self.__len__()
        
    def setRefValues(self, Gamma=None, Momentum=None, Energy=None, Beta=None, Mass=None, Charge=None):
        if  (not Gamma is None):         
            self.RefGamma=Gamma
        if  (not Momentum is None):         
            self.RefMomentum=Momentum
        if  (not Energy is None):         
            self.RefEnergy=Energy
        if  (not Beta is None):         
            self.RefBeta=Beta
        if  (not Mass is None):         
            self.RefMass=Mass
        if  (not Charge is None):         
            self.Charge=RefCharge
        self.setRefValuesChanged=True
        
    def updateMap(self):
        for myElement in self:
            if self.setRefValuesChanged:
                myElement.setRefValues(Gamma=self.RefGamma, Momentum= self.RefMomentum, Energy=self.RefEnergy,
                                       Beta=self.RefBeta, Mass=self.RefMass, Charge=self.RefCharge)
            myElement.updateMap()
        self.setRefValuesChanged=False
            
    def Track(self, beam, offset=np.array([0, 0, 0, 0, 0, 0])):
        self.updateMap()    #make sure all magnets use same momentum, mass, etc
        for ele in self:
            ele.TrackThroughElement(beam)
            
    def getMapFull(self):
        M = np.array([[1,0,0,0,0,0],
                      [0,1,0,0,0,0],
                      [0,0,1,0,0,0],
                      [0,0,0,1,0,0],
                      [0,0,0,0,1,0],
                      [0,0,0,0,0,1]],dtype = np.float_)
        self.updateMap()    #make sure all magnets use same momentum, mass, etc
        for ele in self:
            M=np.dot(ele.getMap(), M)
        return M
   
def getTwiss(SubMap):
    """Calculate TWISS parameter from a 2x2 Matrix
    returns a 3 tuple of (alpha, beta, gamma)"""
    cosmu=(SubMap[0,0]+SubMap[1,1])/2
    alpha=( SubMap[0,0]-cosmu) / math.sqrt(1-cosmu**2)
    beta= ( SubMap[0,1]      ) / math.sqrt(1-cosmu**2)
    gamma=(-SubMap[1,0]      ) / math.sqrt(1-cosmu**2)
    return (alpha, beta, gamma)
        
# A usage example
        # runfile('task_5_sol.py')
        
if __name__ == '__main__':
    pass

    GlobalRefGamma=10
    GlobalRefMass=proton_mass
    GlobalRefCharge=e_charge
    
    f0=math.sqrt(2)
    L0=1.0
    myFODO = Line()
    myFODO.append(QuadrupoleThin(f=2*f0))
    myFODO.append(DriftSpace( L0))
    myFODO.append(QuadrupoleThin(f=-f0))
    myFODO.append(DriftSpace( L0))
    myFODO.append(QuadrupoleThin(f=2*f0))    
    
#Assignment 7 Question 5.1   
    if 1==1:
        myBeam=Beam()
        myBeam.AddParticle(1,1,1.2,1.2,1.3,0.03)
        myBeam.PlotTriple(1)
        plt.suptitle('Random particle while going 80 times through FODO')
        for x in range(80):
            myFODO.Track(myBeam)
            myBeam.PlotTriple(1)
            if (x % 5 ==0) :
                plt.pause(0.5)
            
#Assignment 7 Question 5.2   
    if 1==1:
        myBeam2=Beam()
        myBeam2.AddParticle(0.5,0.5,1.2,1.2,1.3,0.03)
        myBeam2.PlotTriple(1)
        plt.suptitle('Other particles while going 80 times through FODO')
        for x in range(80):
            myFODO.Track(myBeam2)
            myBeam2.PlotTriple(1)
        plt.pause(0.1)
    
        myBeam3=Beam()
        myBeam3.AddParticle(1,-1,1.2,1.2,1.3,0.03)
        myBeam3.PlotTriple(1)
        for x in range(80):
            myFODO.Track(myBeam3)
            myBeam3.PlotTriple(1)
        plt.pause(0.1)
        
        myBeam4=Beam()
        myBeam4.AddParticle(0,5,1.2,1.2,1.3,0.03)
        myBeam4.PlotTriple(1)
        for x in range(80):
            myFODO.Track(myBeam4)
            myBeam4.PlotTriple(1)
        plt.pause(10)

#Assignment 7 Question 5.3       
    if 1==1:
        # Twiss analysis
        Map=myFODO.getMapFull()
        R2x=Map[0:2,0:2]
        R2y=Map[2:4,2:4]
        (alpha_x,beta_x,gamma_x) = getTwiss(R2x)
        (alpha_y,beta_y,gamma_y) = getTwiss(R2y)
        J=3
        if beta_x*gamma_x > 1:
            phi_x=np.arccos(1/math.sqrt(beta_x*gamma_x))
        else:
            phi_x=0
        
        if (-alpha_x/beta_x < 0):
            phi_x = - phi_x
        
        if beta_y*gamma_y > 1:
            phi_y=np.arccos(1/math.sqrt(beta_y*gamma_y))
        else:
            phi_y=0
        
        if (-alpha_y/beta_y < 0):
            phi_y = - phi_y
        
        myBeam=Beam()
        myBeam.BeamPairwiseEllipseLine(20, 
              math.sqrt(2*beta_x*J), math.sqrt(2*gamma_x*J), phi_x,
              math.sqrt(2*beta_y*J), math.sqrt(2*gamma_y*J), phi_y,
              0,0,0)
        plt.clf()
        myBeam.PlotTriple(1)
        plt.suptitle('particle distribution according to TWISS parameters')
        plt.pause(2)      
        print "iteration 1"
        myFODO.Track(myBeam)
        myBeam.PlotTriple(1)
        plt.suptitle('particle distribution according to TWISS parameters\n'+
                'initial distribution + after one pass through FODO')
        plt.pause(3)

#Assignment 7 Question 5.5
    if 1==1:
        myBeam6=Beam('normal',200, 1.4,1, 1.9,0.2, 1.3,0.03, 
                      0.1, 0.1, 0.1) 
        myBeam6.PlotTriple(3)
        myFODO.Track(myBeam6) 
        myBeam6.PlotTriple(3)
        plt.suptitle('gaussian particle distribution')
        for x in range(80):
            myFODO.Track(myBeam6)
        myBeam6.PlotTriple(4)       
        plt.pause(3)
  
    if not matplotlib.is_interactive():
        print "You can have now a look on the plots,"
        print "and e.g., use the zooming features."
        print "once you are finished, close all plot-windows to continue this script"
    plt.show()

print " task_5_sol.py finished"    
#This file illustrates the usage of the file task_5_sol.py
#but now we want to have a separate file, and import the library only.
#Thus we have to use a slightly different syntax than inside the task_5_sol.py examples

#Additionally it gives some other physics examples


#First import the library      
import task_5_sol 

#also import some other libraries (just in case we are in bare python (no scipy environment)
import math
import matplotlib.pyplot as plt 
import numpy as np 

#For this exercise we do not want to mess around with units or values of mass, beta energy or momentum
#as we consider a perfect FODO cell, those do affect only the longitudinal movement which we gonna ignore anyway.
#thus we set some arbitraty default (so we will not get any warning)
task_5_sol.GlobalRefGamma=10
task_5_sol.GlobalRefMass=task_5_sol.proton_mass
task_5_sol.GlobalRefCharge=task_5_sol.e_charge


#This is the FODO-cell of eq13 (as intended by exercise):
f0=math.sqrt(2)
L0=1.0
        
myFODO = task_5_sol.Line()
myFODO.append(task_5_sol.QuadrupoleThin(f=2*f0))
myFODO.append(task_5_sol.DriftSpace( L0))
myFODO.append(task_5_sol.QuadrupoleThin(f=-f0))
myFODO.append(task_5_sol.DriftSpace( L0))
myFODO.append(task_5_sol.QuadrupoleThin(f=2*f0))

myBeam=task_5_sol.Beam()
myBeam.AddParticle(1,0,1,0,0,0)
myBeam.PlotTriple(1)

for x in range(40):
    myFODO.Track(myBeam)
    myBeam.PlotTriple(1)
    if (x % 1 ==0) :
        plt.pause(0.01)         

#TWISS analysis
plt.clf()

Map=myFODO.getMapFull()
R2x=Map[0:2,0:2]
R2y=Map[2:4,2:4]
(alpha_x,beta_x,gamma_x) = task_5_sol.getTwiss(R2x)
(alpha_y,beta_y,gamma_y) = task_5_sol.getTwiss(R2y)
J=3
phi_x=np.arccos(1/math.sqrt(beta_x*gamma_x))
if not np.isfinite(phi_x):
    phi_x=0

if (-alpha_x/beta_x < 0):
    phi_x = - phi_x

phi_y=np.arccos(1/math.sqrt(beta_y*gamma_y))
if not np.isfinite(phi_y):
    phi_y=0

if (-alpha_y/beta_y < 0):
    phi_y = - phi_y

myBeam=task_5_sol.Beam()
myBeam.BeamPairwiseEllipseLine(20, 
      math.sqrt(2*beta_x*J), math.sqrt(2*gamma_x*J), phi_x,
      math.sqrt(2*beta_y*J), math.sqrt(2*gamma_y*J), phi_y,
      0,0,0)
myBeam.PlotTriple(1)
plt.pause(2)      
print "iteration 1"
myFODO.Track(myBeam)
myBeam.PlotTriple(1)


print "again Twiss analysis, \n" +\
      "but now starting at different point of FODO cell"
        
myFODO2 = task_5_sol.Line()
#set some arbitrary values for different particle properties
#only to show syntax
#myFODO2.setRefValues(Mass= 0.5e6/task_5_sol.sol()**2)
#myFODO2.setRefValues(Momentum=144 /task_5_sol.sol())
#same cell, but now starting in mid of driftspace
myFODO2.append(task_5_sol.DriftSpace( L0/2))
myFODO2.append(task_5_sol.QuadrupoleThin(f=-f0))
myFODO2.append(task_5_sol.DriftSpace( L0))
myFODO2.append(task_5_sol.QuadrupoleThin(f=f0))
myFODO2.append(task_5_sol.DriftSpace( L0/2))
Map=myFODO2.getMapFull()
R2x=Map[0:2,0:2]
R2y=Map[2:4,2:4]
(alpha_x,beta_x,gamma_x) = task_5_sol.getTwiss(R2x)
(alpha_y,beta_y,gamma_y) = task_5_sol.getTwiss(R2y)
J=3
phi_x=np.arccos(1/math.sqrt(beta_x*gamma_x))
if not np.isfinite(phi_x):
    phi_x=0

if (-alpha_x/beta_x < 0):
    phi_x = - phi_x

phi_y=np.arccos(1/math.sqrt(beta_y*gamma_y))
if not np.isfinite(phi_y):
    phi_y=0

if (-alpha_y/beta_y < 0):
    phi_y = - phi_y

myBeam=task_5_sol.Beam()
myBeam.BeamPairwiseEllipseLine(20, 
      math.sqrt(2*beta_x*J), math.sqrt(2*gamma_x*J), phi_x,
      math.sqrt(2*beta_y*J), math.sqrt(2*gamma_y*J), phi_y,
      0,0,0)
plt.clf()      
myBeam.PlotTriple(1)
for x in range(5):
    plt.pause(2)      
    print "iteration " + str(x)
    myFODO2.Track(myBeam)
    myBeam.PlotTriple(1)

print "10s break to view the results"    
plt.pause(10)


plt.clf()
myBeam2=task_5_sol.Beam()
myBeam2.AddParticle(1,1,1.2,1.2,1.3,0.03)
myBeam2.PlotTriple(1)
plt.suptitle('Random particle while going 80 times through FODO')
print "Random particle while going 80 times through FODO"    
for x in range(80):
    myFODO2.Track(myBeam2)
    myBeam2.PlotTriple(1)
    if (x % 5 ==0) :
        plt.pause(0.1)

myBeam3=task_5_sol.Beam()
myBeam3.AddParticle(1,-1,1.2,-1.2,1.3,0.03)
myBeam3.PlotTriple(1)
plt.suptitle('Another Random particle while going 80 times through FODO')
print "Another Random particle while going 80 times through FODO"
for x in range(80):
    myFODO2.Track(myBeam3)
    myBeam3.PlotTriple(1)
    if (x % 5 ==0) :
        plt.pause(0.1)

print "task_5_examples finished"

# Author:   Nick Sauerwein, Matthias Frey
# Date:     28. August 2016
#
# This is the main file of the AcceLegoRator
# tool.
#
# General procedure:
#   1.  Set some paramterers, like
#       energy, etc.
#   2.  Specifiy elements in a list according
#       to their appearance in the beamline.
#   3.  Create the accelerator object
#   4.  Create bunch
#   5.  Run the application
#
# Call by:
#   python3.5 pyAcceLEGOrator.py
#

import Global as g
from Elements import *
from Bunch import *
from Accelerator import *
from Window import *

if __name__ == "__main__":
    
    # -----------------------------------------------
    # global parameters
    # -----------------------------------------------
    np.set_printoptions(precision=4, suppress=False)
    
    # number of lattice repetitions
    g.nrep = 1
    
    # ticks [-xlim, xlim] x [-ylim, ylim] for plotting
    g.xlim = 50 # mm
    g.ylim = 50 # mm
    
    
    
    g.ekin = 230.14
    g.mass = Constants.pmass
    
    # relativistic factor
    gamma_0 = Physics.getGamma(g.ekin, g.mass)
    
    print('Ekin     = ', g.ekin)
    print('gamma_0  = ', gamma_0)
    print('mass     = ', g.mass)
    
    
    # -----------------------------------------------
    # Define elements and create the accelerator
    # object
    # -----------------------------------------------
    #accelerator = [Quadrupole(0.25, 8.9102, 0),
                   #Drift(1.0),
                   #Dipole(2.5, 1.39723, 0, 0.5),
                   #Drift(1.0),
                   #Quadrupole(0.5, 8.2400, -0.5 * np.pi),
                   #Drift(1.0),
                   #Dipole(2.5, 1.39723, 0, 0.5),
                   #Drift(1.0),
                   #Quadrupole(0.25, 8.9102, 0)]
                   
    elements = [Drift(0.2),
                Quadrupole(0.12, 6.44, -0.5 * np.pi),
                Drift(0.2),
                Quadrupole(0.12, 18.75, -0.5 * np.pi),
                Drift(0.2),
                Quadrupole(0.12, 38.44, 0.0),
                Drift(0.500),
                Dipole(1.008, 1.8, 0, 0.5),
                Drift(0.2),
                Quadrupole(0.15, 49.92, 0.0),
                Drift(0.100),
                Quadrupole(0.15, 33.31, -0.5 * np.pi),
                Drift(0.2),
                Dipole(1.008, 1.8, 0, 0.5),
                Drift(0.2),
                Quadrupole(0.12, 16.79, 0.0),
                Drift(2.52)]
    
    
    ac = Accelerator(elements)
    
    
    
    
    # -----------------------------------------------
    # bunch specification
    # units:
    # x, y, z in mm
    # px, py, pz in mrad
    # -----------------------------------------------
    nParticles = 1000
    
    #C = np.array([[10*beta(0, ac)[0],0,0,0,0,0],
                  #[0,10*gamma(0, ac)[0],0,0,0,0],
                  #[0,0,10*beta(0, ac)[1],0,0,0],
                  #[0,0,0,10*gamma(0, ac)[1],0,0],
                  #[0,0,0,0,10,0],
                  #[0,0,0,0,0, 1]])
    
    # covariance matrix
    sigma = np.array([[ 16.0, 0.0,  0,  0,  0, 0],
                      [ 0.0, 1.0,  0,  0,  0, 0],
                      [ 0,  0, 16.0,  0.0,  0, 0],
                      [ 0,  0, 0.0, 1,  0, 0],
                      [ 0,  0,  0,  0, 0.709, 0.0],
                      [ 0,  0,  0,  0,  0.0, 0.0981]])
    
    centroid = [0, 0, 0, 0, 0, 0]
    
    # create beam
    bunch = Bunch()
    bunch.gaussian(nParticles, centroid, sigma)
    
    
    # -----------------------------------------------
    # view initialization (not to be changed!)
    # -----------------------------------------------
    plt.close('all')
    w = Window(bunch, ac)
    slider = w.getSlider()
    plt.show()

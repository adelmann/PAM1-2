# Author:   Matthias Frey
# Date:     28. August 2016
#
# This file contains global variables that
# are specified by the user in the main function
# and are needed for initializing the accelerator
# elements.

global ekin     # kinetic energy in MeV
global mass     # particle rest mass
global nrep     # number of repetitions of accelerator setting
global xlim     # x limits for phase space plots
global ylim     # y limits for phase space plots
# Author:   Nick Sauerwein
# Date:     2016
#
# Python naming convention:
#   All functions and variables that are given with two
#   leading underscores "__" should be considered as
#   private member functions, respectively, variables.
#
# Public member functions:
#   - getBunchAtPos
#   - getMapAtPos
#   - plotElements
#   - getLength
#
# Private member functions:
#   - __make
#   - __shift
#   - __getQuantityAtPos
#

from Elements import *
import matplotlib.patches as patches
from numpy.linalg import matrix_power

class Accelerator:
    
    def __init__(self, elements):
        # all accelerator elements
        self.__elements = elements
        
        # total length of accelerator
        self.__length = np.sum(device.getLength()
                               for device in self.__elements)
        
        # stores all matrices in a list
        self.__mats = []
        
        # stores all element distances from the origin in a lists
        self.__distances = [0]
        
        # compute all matrices
        self.__make()
    
    
    # ------------------------------------------------------------
    # return the accelerator length
    # ------------------------------------------------------------
    def getLength(self):
        return self.__length
    
    
    # ------------------------------------------------------------
    # setup all matrices and distances
    # ------------------------------------------------------------
    def __make(self):
        # construct all matrices
        for k in range(len(self.__elements)):
            mat = np.eye(6)
            distance = 0
            
            for device in self.__elements[:k+1]:
                distance += device.getLength()
                mat = mat @ device.get()
                        
            self.__mats = self.__mats + [mat]
            self.__distances = self.__distances + [distance]
    
    
    # ------------------------------------------------------------
    # shift in case of periodic lattice
    # @param pos is the longitudinal position [m]
    # @param quantity is the parameter to "shift"
    # ------------------------------------------------------------
    def __shift(self, pos, quantity):
        quantity = pquantity @ matrix_power(self.__mats[-1],
                                            int(pos / self.__length)
                                            )
        pos = pos % self.__length
        return quantity
    
    
    # ------------------------------------------------------------
    # compute the quantity at a certain position in the beamline
    # @param pos is the longitudinal position [m]
    # @param quantity is to be computed
    # ------------------------------------------------------------
    def __getQuantityAtPos(self, pos, quantity):
         # is needed in case of g.nrep > 1
        if pos > self.__length:
            quantity = self.__shift(pos, quantity)
            if pos == 0:
                return quantity
            
        k = np.searchsorted(self.__distances,
                            pos, side = 'left') - 1
        
        if pos == 0:
            k = 0
            
        if k != 0:
            quantity = quantity @ self.__mats[k-1]
        
        quantity = quantity @ self.__elements[k].get(pos -
                                                     self.__distances[k])
        
        return quantity
    
    
    # ------------------------------------------------------------
    # returns the bunch at a certain position
    # @param pos is the position in longitudinal direction where to
    #        evaluate [m]
    # @param bunch is the initial bunch
    # ------------------------------------------------------------
    def getBunchAtPos(self, pos, bunch):
        return self.__getQuantityAtPos(pos, bunch)
    
    
    # ------------------------------------------------------------
    # returns the transfer matrix for a certain position
    # @ param pos is the position to which the map should be
    #         computed [m]
    # ------------------------------------------------------------
    def getMapAtPos(self, pos):
        mat = np.eye(6)
        return self.__getQuantityAtPos(pos, mat)
    
    
    # ------------------------------------------------------------
    # plot all the elements of the beamline
    # with the color specified by the elements themselves
    # @param ax is the axes
    # ------------------------------------------------------------
    def plotElements(self, ax):
        x = g.nrep * self.__length
        n = int(x /self.__length) + 1
        for r in self.__length*np.arange(n):
            for k in range(len(self.__elements)):
                ax.add_patch(
                    patches.Rectangle((self.__distances[k] + r, 0),
                                      self.__elements[k].getLength(),
                                      0.5,
                                      color = self.__elements[k].getColor()
                                      )
                    )
        
        # get a set containing all elements of the beamline
        types = set()
        for elem in self.__elements:
            types.add( (elem.getColor(), elem.getType()) )
        
        # add legend to plot
        handle = []
        for t in types:
            handle.append( patches.Patch(color=t[0], label=t[1]) )
        
        ax.legend(handles=handle, loc=1, borderaxespad=0.,
                        bbox_to_anchor=(0., -0.2, 1., 0.02), ncol=len(handle), fontsize=11)

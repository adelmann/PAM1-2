# Author:   Dr. Jochen Krempel
#           Valeria Rizzoglio
#           Dr. Andreas Adelman
#           Matthias Frey
# Date:     17. September 2016
#
# This file defines the basic constants and physic relations
# used in particle accelerator computations.
#

from scipy.constants import codata # see: http://docs.scipy.org/doc/scipy/reference/constants.html

import numpy as np

class Constants:
    clight     = codata.value('speed of light in vacuum')                   # m / s
    echarge    = codata.value('elementary charge')                          # C
    pmass      = codata.value('proton mass energy equivalent in MeV')       # MeV
    emass      = codata.value('electron mass energy equivalent in MeV')     # MeV
    muon       = codata.value('muon mass energy equivalent in MeV')         # MeV
    deuteron   = codata.value('deuteron mass energy equivalent in MeV')     # MeV
    tau        = codata.value('tau mass energy equivalent in MeV')          # MeV
    triton     = codata.value('triton mass energy equivalent in MeV')       # MeV
    alpha      = codata.value('triton mass energy equivalent in MeV')       # MeV


class Physics:
    # return the relativistic factor
    # @param ekin is the kinetic energy in MeV
    # @param epot is the particle rest mass energy in MeV
    def getGamma(ekin, epot):
        return ekin / epot + 1.0
    
    # return beta ( velocity / clight)
    # @param gamma is the relativistic factor
    def getBeta(gamma):
        return np.sqrt( 1.0 - 1.0 / gamma ** 2 )

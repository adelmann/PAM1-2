# Author:   Nick Sauerwein, Matthias Frey
# Date:     28. August 2016
#
# This file contains all 6x6 matrices of a particle accelerator. Additional
# elements have to inherit from the base class ElementBase.
# The element classes are implemented in such a way that they do not store the matrices as member variables
# but construct them when calling the getter function.
#
# Python naming convention:
#   All functions and variables that are given with two
#   leading underscores "__" should be considered as
#   private member functions, respectively, variables.
#
# Public member functions of ElementBase:
# - constructor:            initialize beta_0 and gamma_0 (known to all instances)
# - get(length = None):     return the 6x6 matrix at a specified location
# - getType:                return the type of the element as string
# - getLength:              return element length [m]
# - getColor:               return element color (string)
#

from abc import abstractmethod

import numpy as np
from Physics import *

import Global as g  # global parameters specified by user in the main function

# ------------------------------------------------------------------------------------------------------------------------
# Base class of all elements. Additional elements need to inherit from it.
# ------------------------------------------------------------------------------------------------------------------------
class ElementBase:
    
    @abstractmethod
    def __init__(self, length, color):
        # set values of private member variables
        # (are known by all derived object instances)
        self.gamma_0 = Physics.getGamma(g.ekin, g.mass)
        self.beta_0 = Physics.getBeta(self.gamma_0)
        
        # length of element [m]
        self.__length = length
        
        # color of element type (for plotting)
        self.__color = color
        
    
    # -----------------------------------------------------
    # return the 6x6 matrix (the position can be specified)
    # -----------------------------------------------------
    @abstractmethod
    def get(self, length = None):
        pass
    
    
    # -----------------------------------------------------
    # used in Dipole and Quadrupole
    # -----------------------------------------------------
    def rotation(self, phi):
        cs = np.cos(phi)
        s  = np.sin(phi)
        
        return np.array([[cs,   0,  s,  0,  0,  0],
                         [0,    cs, 0,  s,  0,  0],
                         [-s,   0,  cs, 0,  0,  0],
                         [0,    -s, 0,  cs, 0,  0],
                         [0,    0,  0,  0,  1,  0],
                         [0,    0,  0,  0,  0,  1]])

    
    # -----------------------------------------------------
    # return the name of the element
    # -----------------------------------------------------
    @abstractmethod
    def getType(self):
        pass
    
    # -----------------------------------------------------
    # element length
    # -----------------------------------------------------
    def getLength(self):
        return self.__length
    
    
    # -----------------------------------------------------
    # That's the color of each instance of an element.
    # It's used for plotting the beamline.
    # -----------------------------------------------------
    def getColor(self):
        return self.__color


# -------------------------------------------------------------------------------------------------------------------------
# Drift
# -------------------------------------------------------------------------------------------------------------------------
class Drift(ElementBase):
    
    
    def __init__(self, length):
        # call base class constructor (--> initialize beta_0 and gamma_0)
        # set values of private member variables
        ElementBase.__init__(self, length, 'white')
    
    
    # -----------------------------------------------------    
    # return 6x6 matrix
    # @param length specified in [m]
    # -----------------------------------------------------
    def get(self, length = None):
        
        # check if a length is specified, if not take the one of object construction
        if length is None:
            length = self.getLength()
        
        f = length / (self.beta_0 * self.gamma_0) ** 2
        
        return np.array([[1, length, 0, 0, 0, 0],
                         [0, 1, 0, 0, 0, 0],
                         [0, 0, 1, length, 0, 0],
                         [0, 0, 0, 1, 0, 0],
                         [0, 0, 0, 0, 1, f],
                         [0, 0, 0, 0, 0, 1]]).T
    
    
    # -----------------------------------------------------
    def getType(self):
        return "Drift"


# -------------------------------------------------------------------------------------------------------------------------
# Dipole 
# -------------------------------------------------------------------------------------------------------------------------    
class Dipole(ElementBase):
    
    # -----------------------------------------------------
    # @param length specified in [m]
    # @param b0 is the vertical magnetic field strenght [T]
    # @param phi is the rotation angle in [rad]
    # @param scale is the relation between bending angle
    # and edge angle [0, 1]
    # -----------------------------------------------------
    def __init__(self, length, b0, phi, scale):
        
        # call base class constructor (--> initialize beta_0 and gamma_0)
        # set values of private member variables
        ElementBase.__init__(self, length, 'blue')
        
        self.__b0 = b0
        self.__phi = phi
        self.scale = scale
        
    
    # -----------------------------------------------------
    # return 6x6 matrix
    # @param length specified in [m]
    # -----------------------------------------------------
    def get(self, length = None):
        
        # check if a length is specified, if not take the one of object construction
        if length is None:
            length = self.getLength()
        
        c = Constants.clight
        q = Constants.echarge
        
        P_0 = g.mass * 1e6 * q / c * np.sqrt( self.gamma_0 ** 2 - 1) # MeV/c**2
        w = q / P_0 * self.__b0
        
        r = self.rotation(self.__phi)
        r_i = self.rotation(-self.__phi)
        
        k1 = -w * np.tan( self.scale * length * w )
        
        fringe = np.array([[1,      0,  0,  0,  0,  0],
                           [-k1,    1,  0,  0,  0,  0],
                           [0,      0,  1,  0,  0,  0],
                           [0,      0,  k1, 1,  0,  0],
                           [0,      0,  0,  0,  1,  0],
                           [0,      0,  0,  0,  0,  1]])
        
        
        cs   = np.cos(w * length)
        s    = np.sin(w * length)
        f    = length / (self.beta_0 * self.gamma_0) ** 2
        ibet = 1.0 / self.beta_0
        
        
        di = np.array([[cs,         s / w,              0,  0,  0,  (1 - cs) / w * ibet],
                       [-w * s,     cs,                 0,  0,  0,  s * ibet],
                       [0,          0,                  1,  length,  0,  0],
                       [0,          0,                  0,  1,  0,  0],
                       [-s * ibet,  -(1-cs) / w * ibet, 0,  0,  1,  f - (w * length - s) / w * ibet ** 2],
                       [0,          0,                  0,  0,  0,  1]])
        
        return (r @ fringe @ di @ fringe @ r_i).T
    
    
    # -----------------------------------------------------
    def getType(self):
        return "Dipole"


# -------------------------------------------------------------------------------------------------------------------------
# Quadrupole 
# -------------------------------------------------------------------------------------------------------------------------            
class Quadrupole(ElementBase):
    
    # -----------------------------------------------------
    # @param length specified in [m]
    # @param gradB is the magnetic field gradient in [T/m]
    # @param phi is the rotation angle [rad]
    # -----------------------------------------------------
    def __init__(self, length, gradB, phi):
        
        # call base class constructor (--> initialize beta_0 and gamma_0)
        # set values of private member variables
        ElementBase.__init__(self, length, 'red')
        
        self.__gradB = gradB
        self.__phi = phi
            
    
    # -----------------------------------------------------
    # return 6x6 matrix
    # @param length specified in [m]
    # -----------------------------------------------------
    def get(self, length = None):
            
        if length is None:
            length = self.getLength()
            
        q = Constants.echarge
        c = Constants.clight
        P_0 = g.mass * 1e6 * q / c * np.sqrt( self.gamma_0 ** 2 - 1) # MeV/c**2
        
        w = np.sqrt( q /P_0 * self.__gradB )
        
        r = self.rotation(self.__phi)
        r_i = self.rotation(-self.__phi)
            
        cs  = np.cos(w * length)
        s   = np.sin(w * length)
        csh = np.cosh(w * length)
        sh  = np.sinh(w * length)
        f   = length / (self.beta_0 * self.gamma_0 ) ** 2
            
        quad = np.array([[cs,       s / w,  0,      0,      0,  0],
                         [-w * s,   cs,     0,      0,      0,  0],
                         [0,        0,      csh,    sh / w, 0,  0],
                         [0,        0,      w * sh, csh,    0,  0],
                         [0,        0,      0,      0,      1,  f],
                         [0,        0,      0,      0,      0,  1]])
            
        return (r @ quad @ r_i).T
    
    
    # -----------------------------------------------------
    def getType(self):
        return "Quadrupole"


# -------------------------------------------------------------------------------------------------------------------------
# TODO: Add additional elements here
# -------------------------------------------------------------------------------------------------------------------------